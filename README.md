# Informes de practiques
En aquest repositori hi ha tot el contingut, creat per els components del grup E-03, usat per redactar els informes de Laboratori d'Electromagnetisme de segon curs del grau de Física a la Universitat Autònoma de Barcelona.

## Descripció
Al repositori es poden trobar els documents LaTeX usats per redactar els informes finals, les imatges en format *svg* adjuntades, els codis per fer les simulacions i els codis per fer les gràfiques de cada pràctica.

El repositori es divideix en tres carpetes diferents: *src*, *img* i *bin*. A la carpeta *src* es troben tots els documents *tex* amb el text dels informes. Aquest està dividit en diferents fitxers; un fitxer per a cada pràctica i un fitxer principal. A la carpeta *img* hi ha totes les imatges usades en la redacció dels informes en format *svg*. En últim lloc, a la carpeta *bin* hi ha tots els codis de les gràfiques, dels programes opcionals i de les simulacions mostrades.

Per altra banda, també hi ha un fitxer *pdf* amb el resultat final dels informes de les pràctiques.

```
│
├── img
│   ├── practica_1
│   ├── practica_3
│   ├── practica_4
│   ├── practica_5
│   ├── practica_6
│   └── practica_7
├── src
│   ├── main.tex
│   ├── practica_1.tex
│   ├── practica_3.tex
│   ├── practica_4.tex
│   ├── practica_5.tex
│   ├── practica_6.tex
│   └── practica_7.tex
├── bin
│   └── simulacio.py
├── LICENSE
├── README.md
└── document.pdf

```

> **Nota:** Tot el material ha estat pensat per utilitzar-se en *GNU/Linux*, encara que no hi hauria d'haver cap problema en altres sistemes operatius.

## Autors
Els autors del material aquí mostrat són els components del grup E-03 de l'assignatura de Laboratori d'Electromagnetisme. Per qualsevol dubte sobre el material d'aquest repositori, es pot escriure un correu a l'adreça

```
contact-project+kaneda44-informes-de-practiques-46183303-issue-@incoming.gitlab.com
```

## Llicència
Tot el material d'aquest repositori està llicenciat sota la llicència *GNU General Public Licence v3*. Per més informació sobre la llicència veure [la llicència](https://gitlab.com/kaneda44/informes-de-practiques/-/blob/main/LICENSE).
