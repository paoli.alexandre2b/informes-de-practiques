# autor: kaneda - (c) GNU GPL v3
# titol: simulacio practica 1
# camp magnetic fils infinits

import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as ct

# renderitzar el text amb LaTeX
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

d = 2                         # separacio fils
r0 = 1                        # TODO
k = 1/(4*np.pi*ct.epsilon_0)  # constant de Coulomb

# potencial fils
def fil_pos(x, y):
    r = np.sqrt(x**2+y**2)
    V = 2*k*np.log(r0/r)
    return V

def fil_neg(x, y):
    r = np.sqrt(x**2+y**2)
    V = -2*k*np.log(r0/r)
    return V

# linies equipotencials
passos, xmax = 200, 5
pas = np.linspace(-xmax, xmax, passos)
x, y = np.meshgrid(pas, pas)
potencial = fil_pos(x+d, y) + fil_neg(x-d, y)

# dibuix camp
dens_fletxes = 25   # densitat camp de fletxes
pas = np.linspace(-xmax, xmax, dens_fletxes)
xx, yy = np.meshgrid(pas, pas)
potencial_2 = fil_pos(xx+d, yy) + fil_neg(xx-d, yy)

# vectors perpendiculars
grad = np.gradient(potencial_2)
norm = np.linalg.norm(grad, axis = 0)
perp = [np.where(norm==0, 0, i/norm) for i in grad]
dx, dy = perp[0], perp[1]

# definir els fils
fil_1 = plt.Circle((-d, 0), 0.2, color = '#555', zorder=5)
fil_2 = plt.Circle((d, 0), 0.2, color = '#555', zorder=5)

# dibuixar plots
fig, ax = plt.subplots(figsize=(5, 5))
ax.contour(x, y, potencial, 20, linewidths = 1, colors = 'gray', alpha = 0.6,
           linestyles = 'solid')
ax.tick_params(direction = 'in', top = 'true', right ='true')
plt.quiver(xx, yy, dy, dx, color = 'gray', alpha=0.4)
ax.add_patch(fil_1)
ax.add_patch(fil_2)

# eliminar marges i guardar
fig.tight_layout()
fig.savefig('./fils_infinits.pdf')
