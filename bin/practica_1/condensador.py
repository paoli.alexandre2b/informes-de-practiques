# autor: kaneda - (c) GNU GPL v3
# titol: simulacio practica 1
# camp magnetic condensador

import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as ct
from scipy import integrate

# renderitzar el text amb LaTeX
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

d = 2                         # separacio plaques
l = 2                         # longitud plaques
Q = 1                         # carrega plaques
k = 1/(4*np.pi*ct.epsilon_0)  # constant de Coulomb

# potencial plaques
def placa_pos(x, y):
    V = integrate.quad(lambda l: k*Q/((y-l)**2+(x-d)**2)**(0.5), -l, l)
    return V[0]

def placa_neg(x, y):
    V = integrate.quad(lambda l: -k*Q/((y-l)**2+(x+d)**2)**(0.5), -l, l)
    return V[0]

# linies equipotencials
passos, xmax = 200, 5
pas = np.linspace(-xmax, xmax, passos)
x, y = np.meshgrid(pas, pas)
pot_1 = np.vectorize(placa_pos)
pot_2 = np.vectorize(placa_neg)
potencial = pot_1(x, y) + pot_2(x, y)

# dibuix camp
dens_fletxes = 25   # densitat camp de fletxes
pas = np.linspace(-xmax, xmax, dens_fletxes)
xx, yy = np.meshgrid(pas, pas)
potencial_2 = pot_1(xx, yy) + pot_2(xx, yy)

# vectors perpendiculars
grad = np.gradient(potencial_2)
norm = np.linalg.norm(grad, axis = 0)
perp = [np.where(norm==0, 0, i/norm) for i in grad]
dx, dy = perp[0], perp[1]

# dibuixar plots
pos_y = [-l, l]     # coordenades y plaques
pos_x = [d, d]      # coordenades x placa 1
pos_x2 = [-d, -d]   # coordenades y placa 2

fig, ax = plt.subplots()
ax.contour(x, y, potencial, 20, linewidths = 1, colors = 'gray', alpha = 0.6,
           linestyles = 'solid')
ax.plot(pos_x, pos_y, linewidth = 5, color = '#555')
ax.plot(pos_x2, pos_y, linewidth = 5, color = '#555')
ax.tick_params(direction = 'in', top = 'true', right ='true')
plt.quiver(xx, yy, dy, dx, color = 'gray', alpha=0.4)

# eliminar marges i guardar
fig.tight_layout()
fig.savefig('./condensador.pdf')
