# autor: kaneda - (c) GNU GPL v3
# titol: simulacio practica 1
# camp magnetic figura lliure

import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as ct
from scipy import integrate

# renderitzar el text amb LaTeX
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

d = 1                         # longitud plaques interiors
l = 2                         # longitud plaques exteriors
Q = 1                         # carrega plaques
k = 1/(4*np.pi*ct.epsilon_0)  # constant de Coulomb

# calcul del potencial del quadrat exterior
# nota: per facilitar els calculs es divideix el quadrat exterior en quatre
#       plaques independents

# potencial plaques
def placa_vert_drt(x, y):
    V = integrate.quad(lambda b: k*Q/((y-b)**2+(x-l)**2)**(0.5), -l, l)
    return V[0]

def placa_vert_esq(x, y):
    V = integrate.quad(lambda b: k*Q/((y-b)**2+(x+l)**2)**(0.5), -l, l)
    return V[0]

def placa_hor_sup(x, y):
    V = integrate.quad(lambda b: k*Q/((y-l)**2+(x-b)**2)**(0.5), -l, l)
    return V[0]

def placa_hor_inf(x, y):
    V = integrate.quad(lambda b: k*Q/((y+l)**2+(x-b)**2)**(0.5), -l, l)
    return V[0]

# calcul del potencial 'ela' interior
# nota: per facilitar els calculs es divideix la 'ela' interior en dues plaques
#       independents

# potencial plaques
def placa_vert(x, y):
    V = integrate.quad(lambda b: -k*Q/((y-b)**2+(x+d)**2)**(0.5), -d, d)
    return V[0]

def placa_hor(x, y):
    V = integrate.quad(lambda b: -k*Q/((y+d)**2+(x-b)**2)**(0.5), -d, d)
    return V[0]

# linies equipotencials
passos, xmax = 200, 5
pas = np.linspace(-xmax, xmax, passos)
x, y = np.meshgrid(pas, pas)
pot_1 = np.vectorize(placa_vert_esq)
pot_2 = np.vectorize(placa_vert_drt)
pot_3 = np.vectorize(placa_hor_sup)
pot_4 = np.vectorize(placa_hor_inf)
pot_5 = np.vectorize(placa_vert)
pot_6 = np.vectorize(placa_hor)
potencial = pot_1(x, y) + pot_2(x, y) + pot_3(x, y) + pot_4(x, y) + pot_5(x, y) + pot_6(x, y)

# dibuix camp
dens_fletxes = 25   # densitat camp de fletxes
pas = np.linspace(-xmax, xmax, dens_fletxes)
xx, yy = np.meshgrid(pas, pas)
potencial_2 = pot_1(xx, yy) + pot_2(xx, yy) + pot_3(xx, yy) + pot_4(xx, yy) + pot_5(xx, yy) + pot_6(xx, yy)

# vectors perpendiculars
grad = np.gradient(potencial_2)
norm = np.linalg.norm(grad, axis = 0)
perp = [np.where(norm==0, 0, i/norm) for i in grad]
dx, dy = perp[0], perp[1]

# dibuixar plots
fig, ax = plt.subplots(figsize=(6,6))
ax.contour(x, y, potencial, 20, linewidths = 1, colors = 'gray', alpha = 0.6,
           linestyles = 'solid')
ax.plot([l, l], [-l, l], linewidth = 5, color = '#555')
ax.plot([-l, -l], [-l, l], linewidth = 5, color = '#555')
ax.plot([-d, -d], [-d, d], linewidth = 5, color = '#555')
ax.plot([-d, d], [-d, -d], linewidth = 5, color = '#555')
ax.plot([-l, l], [-l, -l], linewidth = 5, color = '#555')
ax.plot([-l, l], [l, l], linewidth = 5, color = '#555')
ax.tick_params(direction = 'in', top = 'true', right ='true')
plt.quiver(xx, yy, dy, dx, color = 'gray', alpha=0.4)

# eliminar marges i guardar
fig.tight_layout()
fig.savefig('./fig_lliure.pdf')
