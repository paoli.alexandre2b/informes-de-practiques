# autor: victor rodriguez - (c) GNU GPL v3.0
# apartat opcional practica 7
# simulacio camp magnetic bobina

import numpy as np
import matplotlib.pyplot as plt
import plotly.io as pio
import plotly.graph_objects as go
import sympy as smp
from scipy.integrate import quad
from sympy.vector import cross

# renderitzar el text amb LaTeX
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
pio.renderers.default = 'browser'

#------------------------------------------------------------------------------
# DEFINICIO VARIABLES
# nota: quant major sigui el valor de 'def_bob' mes circular sera la bobina,
#       pero el renderitzat trigara mes. Valor recomanat: 15 - 30

I = 5                  # intensitat de corrent
N = 60                 # nombre de voltes
L = 60                 # longitud de la bobina
R = 4.45               # radi de la bobina
mu = 4*np.pi*10**(-7)  # permeabilitat al buit
def_bob = 25           # definicio de la bobina
GruixEspira = 8        # nomes afecta al dibuix
MidaFletxes = 1        # mida representacio camp

base, alt = 1.8*R, L   # limits regio camp calculat
densx, densz = 8, 10   # densitat de fletxes

punts = def_bob*N      # punts per representar bobina

#------------------------------------------------------------------------------
# FORMA BOBINA
theta = np.linspace(0, 2*np.pi*N, punts)

def bobina(theta):
    return np.array([R*np.cos(theta), R*np.sin(theta),
                     (theta-np.pi*N)*L/(2*np.pi*N)])

lx, ly, lz = bobina(theta)

# calcular diferencial de camp
t, x, y, z = smp.symbols('t, x, y, z')
l = smp.Matrix([R*smp.cos(t), R*smp.sin(t), (t-smp.pi*N)*L/(2*smp.pi*N)])
r = smp.Matrix([x, y, z])
var = r-l

dB = (mu/4*np.pi)*I*smp.diff(l, t).cross(var)/var.norm()**3

# integrar el camp per components
dBx = smp.lambdify([t, x, y, z], dB[0])
dBy = smp.lambdify([t, x, y, z], dB[1])
dBz = smp.lambdify([t, x, y, z], dB[2])

def B(x, y, z):
  return np.array([quad(dBx, 0, 2*np.pi*N, args=(x, y, z), limit=900)[0],
                   quad(dBy, 0, 2*np.pi*N, args=(x, y, z), limit=900)[0],
                   quad(dBz, 0, 2*np.pi*N, args=(x, y, z), limit=900)[0]])

x = np.linspace(-base, base, densx)
z = np.linspace(-alt, alt, densz)
xv, yv, zv = np.meshgrid(x, x, z)

camp_B = np.vectorize(B, signature='(),(),()->(n)')(xv, yv, zv)
Bx = camp_B[:,:,:,0]
By = camp_B[:,:,:,1]
Bz = camp_B[:,:,:,2]

#------------------------------------------------------------------------------
# CORRECCIONS DEL CAMP
# nota: aquesta part es important, ja que al fer el 'meshgrid' hi ha punts que
#       queden just a la bobina i les fletxes resultants distorsionen la imatge

min_camp, max_camp  = -3, 3

Bx[Bx>max_camp] = max_camp
By[By>max_camp] = max_camp
Bz[Bz>max_camp] = max_camp

Bx[Bx<min_camp] = min_camp
By[By<min_camp] = min_camp
Bz[Bz<min_camp] = min_camp

#------------------------------------------------------------------------------
# PLOT DEL RESULTAT

layout = go.Layout(title=r'<b>Representació del camp magnètic</b>',
                   font_color = 'rgb(250,250,250)',
                   paper_bgcolor = 'rgba(30,30,30,1)',
                   scene=dict(aspectratio = dict(x=1, y=1, z=1),
                              camera_eye  = dict(x=1.2, y=1.2, z=1.2)))

dades = go.Cone(x=xv.ravel(), y=yv.ravel(), z=zv.ravel(), u=Bx.ravel(),
               v=By.ravel(), w=Bz.ravel(), colorscale='Ice',
               sizemode="scaled", sizeref=MidaFletxes,
               colorbar=dict(title=r'Inducció magnètica (T)',
                             titleside='right'))

fig = go.Figure(data = dades, layout=layout)
fig.update_scenes(xaxis_visible=False, yaxis_visible=False, zaxis_visible=False)
fig.add_scatter3d(x=lx, y=ly, z=lz, mode='lines',
                  line = dict(color='gray', width=GruixEspira))

fig.show(config={"displayModeBar": False, "showTips": False})
