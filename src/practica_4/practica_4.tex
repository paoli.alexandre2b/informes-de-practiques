% author: kaneda
% contingut: practica 4
% titol: inductancia mutua i transformadors

\newcommand{\den}{\sqrt{Z^2 + X^2\alpha^4(k^2 - 1)^2}}

\title{Inductància mútua i transformadors}
\date{23 de març del 2023}
\maketitle

\begin{abstract}
	L'objectiu d'aquesta pràctica és estudiar el fenomen de la inductància
	mútua i com aquesta és la base del funcionament d'un transformador.  En la
	pràctica es comprova la relació del voltatge d'entrada i de sortida en
	funció de la posició de les bobines primària i secundària al transformador,
	i en funció del nombre de voltes de cada bobina. Endemés la realització i
	construcció d'un seguit d'estructures metà\lgem iques on s'han co\lgem ocat
	les bobines permet estudiar amb més detalls la inducció magnètica.
\end{abstract}

\section{Introducció i objectius} \label{induc}

\subsection{Fonament teòric} \label{induc:teoria}
Quan en un circuit tancat es produeix una variació de flux d'inducció
magnètica, aquest crea una força electromotriu induïda al circuit conforme a la
següent equació
\begin{equation}
    \oint \vec{E} \cdot \vec{dl} = -\frac{d\Phi}{dt} \; .
\end{equation}

Aquest fenomen també s'observa en l'altre sentit; quan en un circuit hi circula
un corrent $I$ que varia amb el temps, apareix un flux d'inducció magnètica que
és proporcional a aquest corrent $I$. Matemàticament es pot expressar segons
\begin{equation}
    \Phi = L I \; ,
\end{equation}
on es defineix $L$ com l'autoinductància del circuit i depèn únicament de la
geometria del circuit i de la permeabilitat del medi. Ara, si s'acosta un segon
circuit al primer, el corrent que transcorre pel primer circuit generarà un flux al
segon. Definint $M$ com la inductància mútua, s'expressa segons
\begin{equation}
    \Phi_2 = MI_1 \; .
\end{equation}

Una de les principals aplicacions de la inductància es troba en transformadors,
uns components elèctrics que permeten augmentar o disminuir la tensió en un
circuit de corrent alterna, mantenint constant la potència. Esquemàticament, es
poden representar amb dues bobines enrotllades sobre un nucli tancat d'algun
material ferromagnètic. En un cas ideal, tot el flux generat per la primera
bobina passa per la segona i viceversa. Per tant, si per la primera bobina
circula un corrent $I_1$ es té
\begin{equation}
	\frac{\Phi_1}{\Phi_2} = \frac{L_1I_1}{MI_1} = \frac{L_1}{M} =
	\frac{n_1}{n_2} \; ,
    \label{eq:flux_I1}
\end{equation}
on $n_1$ i $n_2$ son el nombre de voltes de la bobina primària i secundària,
respectivament. Per a un corrent $I_2$ que circula per la segona bobina es té
\begin{equation}
	\frac{\Phi_2}{\Phi_1} = \frac{L_2I_2}{MI_2} = \frac{L_2}{M} =
	\frac{n_2}{n_1}
    \label{eq:flux_I2}
\end{equation}
i per consegüent, la relació entre els voltatges és
\begin{equation}
    \frac{V_1}{V_2} = \frac{d\Phi_1 / dt}{d\Phi_2 / dt} = \frac{n_1}{n_2} \; .
\end{equation}

D'acord amb les equacions (\ref{eq:flux_I1}) i (\ref{eq:flux_I2}), teòricament
la inductància mútua estaria definida com l'arrel quadrada del producte de les
autoinductàncies, però experimentalment, no tot el flux creat per una bobina
passa per l'altra. És per això que es defineix una constant $k$ anomenada
coeficient d'acoblament, que va de zero a la unitat. Es defineix, llavors, la
inductància mútua segons
\begin{equation}
    M = k (L_1L_2)^{1/2} \; .
    \label{eq:ind_mutua_exp}
\end{equation}

\begin{figure*}[ht]
	\centering
	\includegraphics[width=\textwidth]{practica_4/Dibuix_v2.pdf}
	\caption{Esquema del muntatge de les configuracions de la pràctica}
	\label{fig:4:muntatge}
\end{figure*}

Per aconseguir que les pèrdues de flux magnètic siguin mínimes, i així
maximitzar el valor de la contant $k$, es co\lgem oca un material d'alta
permeabilitat al centre de les bobines.

Per altra banda, el transformador també es pot estudiar com a part d'un circuit
amb una impedància de càrrega $Z$ connectada al secundari. Emprant las lleis de
Kirchhoff s'obté
\begin{gather}
    V_1 = I_1 R_1 + i \omega L_1 I_1 + i \omega M I_2 , \\
    0 = I_2 R_2 + i \omega L_2 I_2 + i \omega M I_1 + I_2 Z .
\end{gather}
on $R_1$ i $R_2$ són les resistències òhmiques de les dues bobines. Del sistema
anterior s'obtenen les següents solucions per les intensitats, que venen
donades segons
\begin{equation}
    I_1 = \frac{Z + R_2 + iX_2}{(R_1 + iX_1)(Z + R_2 + iX_2) + k^2X_1X_2} V_1 ,
\end{equation}
\begin{equation}
    I_2 = \frac{-ik\sqrt{X_1X_2}}{(R_1 + iX_1)(Z + R_2 + iX_2) + k^2X_1X_2} V_1
\end{equation}
on s'han definit les reactàncies de les bobines com $X_{1,2} = \omega L_{1,2}$.
Sempre que es compleixi que les reactància de cada bobina és molt més gran que
les respectives resistències, les solucions es poden simplificar en tres casos
destacables.

Per a una impedància de càrrega molt gran, és a dir, per a una impedància de
càrrega molt més gran que les reactàncies de les bobines, s'obté
\begin{equation}
    |I_1| = \frac{|V_1|}{X}
    \label{eq:reactancia}
\end{equation}
\begin{equation}
    |V_2| = k\alpha|V_1|
    \label{eq:Z>X}
\end{equation}

Si pel contrari la impedància de càrrega és molt petita, és a dir, la
impedància de càrrega és molt més petita que les reactàncies de les bobines,
s'expressen segons
\begin{equation}
    |V_2| = \frac{k \alpha |Z| |V_1|}{\den} .
    \label{eq:Z<X}
\end{equation}

Per últim, si la impedància de càrrega és de l'ordre de la impedància del
secundari, és a dir, si la impedància és aproximadament igual a les reactàncies
de les bobines es pot escriure
\begin{equation}
    |V_2| = k\alpha |V_1|
    \label{eq:Z-X}
\end{equation}

\section{Mètode experimental} \label{induc:metode}
Aquesta pràctica tracta l’estudi experimental de fenòmens relacionats amb la
inducció electromagnètica, posant especial`` èmfasi en el concepte d’inducció
mútua i en l’estudi de transformadors. Per a la realització experimental de la
pràctica es disposa de diverses bobines amb diferent nombre de voltes i de les
mateixes dimensions que, conseqüentment, produiran diferents camps magnètics
per un mateix valor d'intensitat subministrada.

Pel que fa al material del que es disposa en la realització d'aquesta pràctica,
l'objecte principal són les bobines de diferent nombre de voltes. Com s'ha
comentat, aquestes s'utilitzen per crear els transformadors i estudiar el
fenomen de l'autoinductància mútua, tant amb ferros com en part d'un circuit.
D'altra banda, també es disposa d'un seguit de peces de ferro amb geometries
variades que són usades per a recollir les línies d'inducció magnètica i són
emprades a la primera part.\footnote{Vegeu la figura \ref{fig:4:muntatge}, on
es mostren diferents configuracions creades a partir d'aquestes peçes.}
Finalment, es disposa de tres resistències de valors de 10 $\Omega$, 100
$\Omega$ i 1000 $\Omega$, respectivament. Aquestes crearan la impedància de
càrrega a la segona part de la pràctica.

En tenir com a objecte d'estudi diferents objectius a demostrar, la pràctica es
divideix en dues parts; a la primera part es realitza un estudi de les
variables d'un transformador, i a la segona s'estudien les intensitats i els
voltatges d'entrada i de sortida del circuit treballat.

\subsection{Variables d'un transformador}
En la primera part de la pràctica es disposa del circuit mostrat a la figura
\ref{fig:transformador}. Primerament, s'aplica un corrent altern de $6$
\unit{V} a la bobina primària i es mesura el voltatge de sortida de la bobina
secundària. Seguidament s'estudien diferents configuracions tant de disposició
de les bobines com la dependència amb el nombre de voltes.

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.4\textwidth]{practica_4/transformador.png}
    \caption{Esquema de les connexions d'un transformador.
    \texttt{\small[font:guió]}}
    \label{fig:transformador}
\end{figure}

Seguidament, amb les dues bobines de 400 voltes i les configuracions mostrades
a la figura \ref{fig:circuit2} es mesurara el voltatge de sortida de la bobina
secundària. A continuació, amb la quarta configuració de la figura
\ref{fig:4:muntatge} es proven totes les combinacions possibles entre les
bobines d'entre 200 i 3200 voltes i es mesura el voltatge de
sortida.\footnote{Concretament les combinacions són per a 200, 400, 800, 1600 i
3200 voltes.}

Noti's que per aquesta primera part de la pràctica no s'utilitza cap peça
metà\lgem ica de les mencionades anteriorment, ja que la introducció d'aquestes
té lloc a la segona part de la pràctica. La relació i comparació entre els
resultats obtinguts en ambdues parts de la pràctica serà objecte d'estudi.

\subsection{Estudi com a circuit}
En aquesta segona part de la pràctica es passa a estudiar el transformador com
a element o component d'un circuit i s'utilitza un amperímetre per mesurar-ne
les diferents intensitats. Es realitza un estudi dels paràmetres a partir de la
connexió d'una resistència al circuit secundari.

Primerament es munta el circuit tancat com es mostra a la figura
\ref{fig:circuito}. Es co\lgem oca el transformador amb la bobina primària de
400 voltes, la bobina secundària es va canviant per a diferents mesures i, a
més, s'afegeix una resistència de $1000 \; \Omega$. A continuació, es mesuren
les intensitats del circuit primari per a cadascuna de les diferents bobines
secundàries.

Seguidament, amb la mateixa configuració que a la figura \ref{fig:circuito}, es
deixa en aquest cas el circuit obert i es mesura el voltatge, $V_1$, i la
intensitat, $I_1$, del primari co\lgem ocant una bobina de 400 voltes al
primari. A la bobina secundària, es co\lgem oquen les bobines de 400 i 800
voltes, per tal de mesurar la reactància de cadascuna. Com s'ha observat a la
introducció teòrica, aquesta situació equival a tenir una resistència infinita
al secundari, pel que es correspon a la primera situació comentada a l'apartat
\textit{Fonament teòric}.

Per acabar, un cop calculades les reactàncies, es tanca el circuit i es co\lgem
oca una resistència. El valor d'aquesta resistència es va modificant, usant
resistències de $10 \Omega$, $100 \Omega$ i $1000 \Omega$, igual que també
varia el nombre de voltes de la bobina secundària; usant bobines de 400 i 800
voltes. Amb aquests 6 casos, es mesura el votatge d'entrada, $V_1$, el voltatge
de sortida, $V_2$, la intensitat d'entrada, $I_1$ i la intensitat de sortida,
$I_2$, amb la finalitat de calcular el guany de voltatge experimentalment.
Prèviament cal haver fet els càlculs teòrics per a aquest guany seguint les
equacions (\ref{eq:Z>X}), (\ref{eq:Z<X}), i (\ref{eq:Z-X}) prèviament
introduïdes per tal de poder comparar els resultats experimentals amb les
prediccions teòriques.

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.4\textwidth]{practica_4/circuito.png}
    \caption{Esquema del circuit del transformador amb una resistència.
    \texttt{\small[font:guió]}}
    \label{fig:circuito}
\end{figure}

\section{Resultats i discussió} \label{induc:resultats}
Es comença per l'estudi de la variació del voltatge de sortida depenent de la
configuració i de la geometria d'aquesta. A la taula \ref{tab:guany_conf} es
mostren els valors de voltatge d'entrada, $V_1$, i del voltatge de sortida,
$V_2$, mesurats i el guany de voltatge $V_2/V_1$ calculat. S'observa com en
afegir el material d'alta permeabilitat, augmenta considerablement el nombre de
línies de camp recollides. La configuració que té més guany ha estat la número
6.\footnote{Veure imatge \ref{fig:4:muntatge} de la pàgina anterior.} Això és
degut a que la geometria del nucli i la disposició de les bobines, és la que
més s'assembla a les línies de camp d'inducció magnètica que crea la bobina
primària, permetent perdre la menor quantitat de flux i apropant-se més a la
situació d'un transformador ideal.

\begin{longtblr}[theme=Personalitzat,
	label={tab:guany_conf},
	caption={Guany del voltatge per a les diferents configuracions.},
	note{*}={Configuració sense ferro.}
		]{
			colspec={X[1] X[3] X[3] X[3]},
			cells={c, m},
			row{even}={bg=gray!5},
			row{1}={font=\small},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
    \textbf{\normalsize Fig.} & ($V_1 \pm 0,05$) (V) & ($V_2 \pm 0,02$) (V) &
    $V_2/V_1 \cdot 10^2$ \\
	\toprule
	0\TblrNote{*} & $6,12$ & $0,25$ & $4,08 \pm 0,33$ \\
    1 & $6,09$ & $2,33$ & $38,26 \pm 0,45$\\
    2 & $6,09$ & $2,11$ & $34,65 \pm 0,43$\\
    3 & $6,10$ & $4,33$ & $70,98 \pm 0,67$\\
    4 & $6,06$ & $5,77$ & $95,21 \pm 0,85$\\
    5 & $6,06$ & $1,45$ & $23,93 \pm 0,38$\\
    6 & $6,14$ & $5,92$ & $96,42 \pm 0,85$\\
	\bottomrule
\end{longtblr}

\begin{table*}
	\centering
    \caption{Guany del voltatge per a diferents bobines amb la configuració 4
    de la figura \ref{fig:4:muntatge}.}
	\label{tab:guany_voltes}
	\begin{tblr}
			{
			cells={c},
			row{even}={bg=gray!5},
			row{2}={bg=white},
			cell{2-Z}{1}={font=\itshape},
			rowhead=2,
		}
		\toprule
		\SetCell[r=2]{t} $\mathbf{N_1}$ & \SetCell[c=5]{c} Bobina secundària \\
		& 200 & 400 & 800 & 1600 & 3200 \\
		\cmidrule{2-Z}
    200 & - & $1,9441 \pm 0,0036$ & $3,819 \pm 0,017$ & $7,750 \pm 0,021$ &
        $15,561 \pm 0,031$ \\
    400 & $0,4575 \pm 0,0018$ & $0,9642 \pm 0,0085$ & $1,8709 \pm 0,0035$ &
        $3,745 \pm 0,017$ & $7,443 \pm 0,20$ \\
    800 & $0,21967 \pm 0,00035$ & $0,4590 \pm 0,018$ & - & $1,8775 \pm 0,0035$
        & $3,739 \pm 0,017$ \\
    1600 & $0,11623 \pm 0,00025$ & $0,23481 \pm 0,00042$ & $0,4614 \pm 0,0018$
        & - & $1,8944 \pm 0,0035$ \\
    3200 & $0,05574 \pm 0,00019$ & $0,11356 \pm 0,00025$ & $0,22787 \pm
        0,00040$ & $0,4836 \pm 0,0018$ & - \\
		\bottomrule
	\end{tblr}
\end{table*}

Un cop trobada la configuració més propera a un transformador ideal, la
configuració 6 de la figura \ref{fig:4:muntatge}, canviant el nombre de voltes
de les bobines s'han obtingut els resultats mostrats a la taula
\ref{tab:guany_voltes}.

Els valors es desvien des d'un 2,8\% el que menys, fins a un 12,1\% el que més.
Es pot veure que, encara que els valors s'apropen als valors esperats,
segueixen sent bastant allunyats dels valors teòrics. La raó d'aquest fenomen
és que en un transformador real sempre es perd una porció del flux magnètic i
en aquesta pràctica no és negligible.

Teòricament s'havia comentat que aquest guany depèn únicament del nombre de
voltes de la bobina, d'acord amb l'equació \ref{eq:guany}, però es considerava
un cas ideal on tot el flux creat per la bobina primària passava per la
secundària. Aquesta és la raó principal que els valors experimentals no entrin
dins el rang d'incerteses dels valors teòrics.

\subsection{Estudi com a circuit}
Seguim ara amb la següent part de la pràctica, l'estudi del transformador en un
circuit. Amb el circuit tancat, el transformador amb la bobina primària de 400
voltes, i una resistència de $1000 \Omega$, es mesuren les intensitats del
circuit primari per a cadascuna de les diferents bobines secundàries. Tal com
es mostra a la taula \ref{tab:I_cambiando_N}, es pot observar com la intensitat
canvia linealment amb el nombre de voltes de la bobina secundària, tal com
s'esperava per teoria.

\begin{longtblr}[theme=Personalitzat,
	label={tab:I_cambiando_N},
    caption={Intensitat bobina primària variant el nombre de voltes de la
    bobina secundària.},
		]{
			colspec={X[1] X[2]},
			cells={c},
			row{even}={bg=gray!5},
			row{1}={font=\bfseries},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
	$n_2$ & $I_1$ (\unit{A}) \\
	\toprule
    200 & $0,03570 \pm 0,00010$ \\
    400 & $0,04810 \pm 0,00010$ \\
    800 & $0,05920 \pm 0,00010$ \\
    1600 & $0,09660 \pm 0,00010$ \\
    3200 & $0,320 \pm 0,010$ \\
	\bottomrule
\end{longtblr}

Amb el circuit obert i la bobina a calcular, es mesura el voltatge d'entrada,
$V_1$, i la intensitat, $I_1$. Partint de les lleis de Kirchhoff, i utilitzant
l'equació (\ref{eq:reactancia}) s'obtenen les reactàncies de les bobines de 400
i 800 voltes, els resultats es poden veure a la taula \ref{tab:reactancies}.

\begin{longtblr}[theme=Personalitzat,
	label={tab:reactancies},
	caption={Reactàncies de les bobines.},
		]{
			colspec={X[1] X[3] X[3] X[3]},
			cells={c, m},
			row{even}={bg=gray!5},
			column{2-Z}={font=\small},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
	\nta{\normalsize N} & ($V_1 \pm 0,02$) (V) & $I_2$ (mA) & $X (\Omega)$ \\
	\toprule
    400 & $6,06$ & $44,5 \pm 0,1$ & $136,25 \pm 0,17$ \\
    800 & $6,01$ & $15,27 \pm 0,02$ & $393,70 \pm 0,35$ \\
	\bottomrule
\end{longtblr}

Utilitzant les dades de la taula \ref{tab:reactancies} i l'equació
(\ref{eq:Z>X}) es calcula la constant d'acoblament del transformador per a cada
bobina secundària. S'obté que les constants són: $k_{400} = (0,9513 \pm 0,0022)$
i $k_{800} = (0,9334 \pm 0,0017)$. Es faràn servir per a calcular el valor
teòric del guany de voltatge al següent apartat.

\begin{table*}
	\centering
    \caption{Guany del voltatge per a diferents bobines en funció de diferents
    resistències i en un circuit tancat.}
	\label{tab:V_I_resist}
	\begin{tblr}
			{
			cells={c},
			row{1-2}={bg=white},
			row{even[3-X]}={bg=gray!5},
			cell{Y-Z}{2-Z}={bg=gray!5},
			cell{Y-Z}{1}={bg=gray!15, font=\bfseries},
			rowhead=2,
		}
		\toprule
        \SetCell[r=2]{t} & \SetCell[c=6]{c} Nombre de voltes bobina secundària
        \\
		\cmidrule{2-Z}
		$\mathbf{N_2}$ & \SetCell[c=3]{c} $400$ & & & \SetCell[c=3]{c} $800$ \\
		\cmidrule[r]{2-4} \cmidrule[l]{5-Z}
        $\mathbf{Z \; (\boldsymbol{\Omega})}$ & $1000 \pm 1$ & $10,0 \pm 0,1$ &
        $100,0 \pm 0,1$ & $1000 \pm 1$ & $10,0 \pm 0,1$ & $100,0 \pm 0,1$ \\
		\midrule
    $V_1 \; (V)$ & $6,16 \pm 0,05$ & $6,11 \pm 0,05$ & $6,16 \pm 0,05$ & $6,16
        \pm 0,05$ & $6,08 \pm 0,05$ & $6,12 \pm 0,05$ \\
    $V_2 \; (V)$ & $5,86 \pm 0,02$ & $3,49 \pm 0,02$ & $5,59 \pm 0,02$ & $11,50
        \pm 0,02$ & $2,69 \pm 0,02$ & $9,46 \pm 0,02$ \\
    $I_1 \; (mA)$ & $44,9 \pm 2,0$ & $320,0 \pm 2,0$ & $68,4 \pm 2,0$ & $54,2 \pm 2,0$ &
        $480,0 \pm 2,0$ & $137,0 \pm 2,0$ \\
    $I_2 \; (mA)$ & $5,13 \pm 2,0$ & $300,0 \pm 2,0$ & $42,9 \pm 2,0$ & $10,1 \pm 2,0$ &
        $230 \pm 2,0$ & $61,1 \pm 2,0$ \\
	\midrule
    Mesurat & $0,95 \pm 0,01$ & $0,57 \pm 0,01$ & $0,91 \pm 0,01$ & $1,87 \pm 0,02$ &
        $0,431 \pm 0,009$ & $1,55 \pm 0,02$\\
    Teòric & $0,98 \pm 0,03$ & $0,59 \pm 0,02$ & $0,95 \pm 0,03$ & $1,867 \pm 0,049$ &
        $0,45 \pm 0,01$ & $1,87 \pm 0,02$\\
		\bottomrule
	\end{tblr}
\end{table*}

A continuació es tanca el circuit i s'hi afegeixen diferents resistències. Per
calcular el guany teòric, s'utilitzen una de les aproximacions esmentades a
l'apartat de fonament teòric. Les resistències que es disposen són de 10
$\Omega$, 100 $\Omega$, i 1000 $\Omega$. Es pot observar que per a la
resistència de 100 $\Omega$, ($Z \approx X$), per a la resistència de 10
$\Omega$, ($Z \ll X$) i per a la resistència de 1000 $\Omega$, ($Z \gg X$). Per
tal de calcular el guany teòric s'utilitzen les equacions (\ref{eq:Z-X}),
(\ref{eq:Z<X}) i (\ref{eq:Z>X}) respectivament. Els valors obtinguts es mostren
a la taula \ref{tab:V_I_resist}. Tret del cas de la bobina secundària de 800
voltes i la resistència de $100 \Omega$, que el percentatge de desviació és
17,2\%, la resta de combinacions, la desviació no supera el 6\%. Tot i així,
els valors experimentals, amb el seu rang d'incertesa, no entren dins d'el rang
de valor teòric això és un resultat esperable i, atesa la naturalesa aproximada
de les equacions emprades, justificable.

Observant la resta de resultats, veiem com la intensitat de la bobina primària
no depèn únicament del nombre de voltes de la bobina secundària, com ja s'havia
vist anteriorment. Sinó que també es veu afectada per la impedància del
circuit. S'observa que la intensitat del primari i la impedància tenen una
dependència inversament proporcional. Quan el valor de la resistència és el
menor, es veu com el valor de la intensitat primària és el màxim comparat amb
la resta i viceversa. Per aquesta raó, es pot afirmar que la intensitat depèn
del nombre de voltes de la bobina secundària i de l'entorn.

\section{Conclusions} \label{induc:conclusio}
Tenint en compte que la suposició que a la bobina secundària li travessa tot el
flux de la bobina primària és una suposició molt ideal i allunyada de la
realitat amb els materials del laboratori, els resultats obtinguts són
coherents amb els esperats. Encara que els resultats no hagin estat del tot
satisfactoris a nivell teòric, s'ha pogut assolir l'objectiu principal que era
estudiar el fenomen de la inductància mútua.

A la primera part de la pràctica, amb la mateixa configuració, i canviant el
nombre de voltes de les bobines, s'ha observat com la inductància mútua depèn
únicament de la geometria del sistema; assolint uns millors resultats per
aquelles configuracions més properes a les línes de camp. Els resultats no han
estat els esperats, però per la mateixa raó esmentada anteriorment. S'ha
considerat un sistema ideal, i no s'ha tingut en compte aquest factor $k$,
introduït a l'equació (\ref{eq:ind_mutua_exp}), que ho fa real. En afegit, s'ha
comprovat i mesurat amb les diferents configuracions proposades, que disposant
el material d'alta permeabilitat de manera que tingui la geometria més propera
a les línies de camp, s'obté el guany més gran de voltatge.

A la segona part de la pràctica, s'ha estudiat el transformador dins un
circuit. S'ha observat en un circuit tancat connectat a un transformador i una
resistència la dependència lineal de la intensitat del circuit primari amb el
nombre de voltes de la bobina secundària. Això és degut a que a la bobina
secundària es genera un camp d'inducció magnètica, i aquest afecta sobre la
bobina primària, generant una \textit{fem} que fa variar la intensitat.

Per acabar, s'ha estudiat un circuit tancat amb un transformador i una
impedància, i s'han variat tant el nombre de voltes de la bobina secundària com
el valor de la impedància per veure la dependència amb aquests. Com passa a la
resta de resultats, els valors experimentals no concorden amb els teòrics. En
aquest cas no és degut al factor $k$, ja que es té en compte, aquesta
discordança és causada, probablement, per l'aproximació a les solucions de les
lleis de Kirchhoff. Tot i així, ometent aquest detall, l'estudi de la
intensitat a la bobina d'entrada ha estat satisfactori.
