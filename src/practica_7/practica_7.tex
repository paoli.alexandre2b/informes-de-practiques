% author: kaneda
% contingut: practica 7
% titol: camps magnetics d'espires i bobines

\title{Camps magnètics d'espires i bobines}
\date{4 de maig del 2023}
\maketitle

\begin{abstract}
	En aquesta pràctica s'estudien els camps magnètics que resulten de fer
	circular un corrent elèctric, amb una intensitat determinada, per bobines
	cilíndriques i espires de diferents geometries. Les mesures es realitzen a
	partir d'un teslàmetre basat en una sonda Hall. Els resultats experimentals
	són analitzats a partir de les fórmules analítiques proporcionades per la
	Magnetostàtica i són comparats amb els casos de bobines i espires ideals.
	Endemés, a partir dels resultats obtinguts es determina la permeabilitat
	del buit.
\end{abstract}

\section{Introducció i objectius} \label{bobines}
Es pot calcular de manera senzilla la inducció magnètica, \nta{B}, creada per
una espira circular per la qual circula un corrent, en qualsevol punt del seu
eix, a partir de la llei de Biot-Savart
\begin{equation}
	\nta{B} = \frac{\mu_0 I}{2} \frac{R^2}{(R^2 + z^2)^{3/2}} \nta{e}_z \; ,
	\label{eq:espira}
\end{equation}
on $\mu_0$ és la permeabilitat del buit, $R$ el radi de l'espira, $I$ la
intensitat del corrent que circula per l'espira, $z$ és la distància del punt
de mesura al centre de l'espira; on la direcció de l'eix $z$ es fa coincidir,
per facilitar els càlculs, amb l'eix de l'espira.\footnote{Una altra
consideració que permet facilitar els càlculs en aquesta pràctica és la
consideració que el centre de l'espira es troba en tot moment en el punt on $z$
és zero. De fet, podem considerar el centre de l'espira com l'origen de
coordenades.} A partir d'aquesta última consideració, \nta{B} és perpendicular
al pla de l'espira en la direcció de l'eix $z$. El sentit del camp d'inducció
magnètica depèn del sentit de gir del corrent a través de l'espira, segons la
regla habitual de la mà dreta o del tirabuixó.

D'aquesta manera es pot comprovar trivialment que, a partir de l'expressió
anterior, el camp d'inducció magnètica al centre d'una espira de radi $R$ on,
tal com s'ha mencionat $z$ és zero, ve donat per l'expressió
\begin{equation}
    \nta{B} = \frac{\mu_0 I}{2 R} \nta{e}_z \; , \label{eq:espira_centre}
\end{equation}

En tenir-se diverses espires molt properes es pot prendre pel càlcul del camp
l’expressió anterior multiplicada pel nombre d’espires, $N$. Anàlogament, també
es podria calcular a partir de l'expressió (\ref{eq:espira}) realitzant les
mateixes consideracions que s'han aplicat a l'equació (\ref{eq:espira_centre}).
És a dir, si es mesura el camp d'inducció magnètica al centre de les espires,
on $z$ és zero, s'obté
\begin{equation}
	\nta{B} = \frac{\mu_0 I N}{2 R} \nta{e}_z \; . \label{eq:n_espira}
\end{equation}
Aquesta expressió és vàlida per a la consideració que la distància entre les
espires és mínima. Es fa una aproximació ideal on les $N$ espires considerades
es troben a la mateixa posició i, en tenir totes les mateixes característiques,
les contribucions al camp total de cada espira es sumen. És important remarcar
aquest aspecte per diferenciar els casos de la mesura del camp d'inducció
magnètica de múltiples espires, tal com es fa en la primera part d'aquesta
pràctica, i el cas de les bobines que s'estudien a la segona part.

Per altra banda, en el cas de les bobines no es pot considerar la situació
anterior i el camp magnètic creat a un punt qualsevol de l'eix d'una bobina
cilíndrica per la que circula un corrent, pot calcular-se senzillament
substituint a l'equació (\ref{eq:espira}) $z$ per $z-z'$, on $z'$ é la posició
que ocupa cadascuna de les espires que formen la bobina. Integrant sobre $z'$
per la longitud de la bobina considerant que el centre d'aquesta es troba a $z$
zero, el resultat ve donat per l'expressió
\begin{equation}
	\begin{split}
		\nta{B} &= \frac{\mu_0 I N}{2 L} \biggl(\frac{z + L/2}{\sqrt{R^2 + (z +
		L/2)^2}} \\
		&- \frac{z - L/2}{\sqrt{R^2 + (z - L/2)^2}}\biggr) \nta{e}_z \; .
	\end{split}
	\label{eq:bobines}
\end{equation}
on $R$ és el radi de la bobina, $L$ és la longitud, $I$ la intensitat de
corrent elèctric que circula per la bobina, $\mu_0$ és la permeabilitat del
buit i $N$ el nombre de voltes de la bobina.

Per tant, a partir d'aquests conceptes introduïts, l'objectiu d'aquesta
pràctica és estudiar el camp magnètic induït per bobines cilíndriques i espires
de diferents geometries en fer circular una intensitat determinada a través
d'elles. L'estudi es duu a terme a partir de la comparació amb els resultats
teòrics extrets a partir de les fórmules analítiques mencionades en aquesta
introducció. En últim terme, el càlcul de la permeabilitat del buit a partir de
les dades experimentals reafirmarà i donarà validesa a la correcció dels
possibles valors experimentals obtinguts.

\section{Mètode experimental} \label{bobina:metode}
La realització d'aquesta pràctica es divideix en dues parts per diferenciar,
tal com s'ha expressat en la introducció teòrica; la mesura del camp d'inducció
magnètica al centre d'espires de diferents geometries i la mesura del camp
d'inducció magnètica a l'eix de diverses bobines amb diferents radis i nombre
de voltes. En ambdues parts, per a la presa presa de dades s'utilitza un
teslàmetre basat en una sonda Hall.

\subsection{Mesura del camp de les espires} \label{bobina:metode:esp}
Per a la realització experimental de la primera part de la pràctica es realitza
el muntatge mostrat a la figura \ref{fig:muntatge}, on es veuen representades
l'espira (6, a la figura) i la sonda (3) sobre una regla graduada (4). La sonda
connectada al teslàmetre (1) és la base del muntatge, ja que és el que permet
realitzar les mesures experimentals del camp d'inducció magnètica.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.48\textwidth]{./practica_7/dibuix-1.pdf}
    \caption{Esquema del muntatge \texttt{\small [font:pròpia]}}
    \label{fig:muntatge}
\end{figure}

Al voler-se mesurar el camp al centre geomètric del pla format per l'espira i
per saber amb la màxima seguretat que així es tracta, es situa la sonda en el
punt on el camp d'inducció magnètica mesurat és màxim en valor
absolut.\footnote{Tot i així, no es pot assegurar amb total exactitud que
la sonda es troba al centre de l'espira.} Un cop co\lgem ocades la sonda i
l'espira en les posicions indicades, i amb la intensitat del generador (2)
nu\lgem a, s'ajusta el zero del teslàmetre. Per fer-ho, després d'haver encès
l'aparell i haver-se esperat uns minuts fins que la mesura d'aquest és estable,
s'ajusta fins que a la pantalla es mostra un zero. Aquest procediment de
reajustament del teslàmetre es repeteix abans de cada una de les mesures
realitzades a la pràctica.

A continuació, un cop muntat el circuit, es fa circular un intensitat de 4A a
través de les tres espires estudiades; una de radi petit, una de radi mitjà i
una de radi gran, i es prenen sengles mesures del camp d'inducció magnètica al
centre de cada una. Endemés, per tal de reduir els possibles errors de mesura
deguts a camps residuals i asimetries, totes les mesures es repeteixen dues
vegades canviant el sentit del corrent. Com a resultat de les mesures es pren
la semi-mitjana, o mitjana dels valors absoluts, de les mesures obtingudes.

Seguidament, seguint un procediment similar al del cas anterior per trobar el
centre dels conjunts d'espires, es mesura el camp magnètic al centre dels
conjunts d'una, dues i tres espires de diàmetre gran. Novament, es torna a
calibrar el teslàmetre al zero abans de prendre cada una de les mesures
corresponents.

\subsection{Mesura del camp de les bobines} \label{bobina:metode:bob}
En aquesta segona part de la pràctica es mesura el camp d'inducció magnètica al
llarg de l'eix de simetria de diferents bobines amb geometries diverses. Per a
la realització d'aquestes mesures es realitza el mateix muntatge mostrat a la
figura \ref{fig:muntatge}, però on s'ha substituït l'espira (6) per cada una de
les bobines corresponents. Novament, en aquesta part de la pràctica és
necessari l'ajustament del zero del teslàmetre abans de cada presa de dades i
la co\lgem ocació adient de la sonda a l'eix de simetria de la bobina a partir
del mateix mètode explicat a l'apartat anterior.

Per a cada una de les bobines estudiades; una de diàmetre de 33mm i 300 voltes,
una de 26mm de diàmetre i 75 voltes, i una de 150mm i 300 voltes, es fa
circular una intensitat de 1A i es mesura el camp d'inducció magnètica en
diversos punts de l'eix de simetria, comparant els valors obtinguts amb els
valors teòrics. Tenint en compte que, a partir de l'equació (\ref{eq:bobines}),
el camp al tram central de les bobines és pràcticament constant i aquest
presenta una gran variació als extrems de les bobines es prenen més mesures en
aquelles seccions on la variació teòrica del camp entre dos punts és
major.\footnote{De forma similar a l'apartat anterior, es considera l'origen de
coordenades de les bobines al centre de la longitud d'aquesta i sobre el seu
eix de simetria.}

\section{Resultats i discussió} \label{bobina:resultats}
Els resultats experimentals extrets a partir de les mesures realitzades al
laboratori són comparats amb els models ideals teòrics explicats a partir de
les equacions de la introducció teòrica. Com la pràctica queda clarament
dividida en dues parts, l'exposició i discussió dels resultats també es
realitzarà en dues parts.

\subsection{Mesura del camp de les espires} \label{bobina:resultats_e}
Per a la primera part de la pràctica, els valors experimentals obtinguts en la
mesura del camp d'inducció magnètica per a les tres espires amb diferents radis
es mostren a la taula \ref{tab:mesures_espires}. On hi ha les mesures del mòdul
del camp magnètic en valor absolut per als dos sentits de corrent mesurats.

\begin{longtblr}[theme=Personalitzat,
	label={tab:mesures_espires},
	caption={Mesures camp espires},
	note{*}={Valors en valor absolut.}
		]{
			cells={c},
			row{even}={bg=gray!5},
			row{1}={font=\bfseries},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
	Radi (\unit{m}) & $\mathbf{I}$ (\unit{A}) & $\mathbf{B}$\TblrNote{*} (\unit{mT}) \\
	\toprule
	\SetCell[r=2]{c} $0,030 \pm 0,001$ & $4,00  \pm 0,01 $ & $0,08 \pm 0,01$ \\
	$0,030 \pm 0,001$ & $-4,00 \pm 0,01 $ & $0,08 \pm 0,01$ \\
	\SetCell[r=2]{c} $0,042 \pm 0,001$ & $4,00  \pm 0,01 $ & $0,07 \pm 0,01$ \\
	$0,042 \pm 0,001$ & $-4,00 \pm 0,01 $ & $0,05 \pm 0,01$ \\
	\SetCell[r=2]{c} $0,055 \pm 0,001$ & $4,00  \pm 0,01 $ & $0,05 \pm 0,01$ \\
	$0,055 \pm 0,001$ & $-4,00 \pm 0,01 $ & $0,03 \pm 0,01$ \\
	\bottomrule
\end{longtblr}

Tal com s'ha explicat a la metodologia, per a la mesura del camp en cada una de
les espires es realitzen dues mesures invertit el sentit del corrent i es
realitza la mitjana del mòdul dels valors obtinguts per al camp d'inducció
magnètica. Aquesta metodologia permet reduir errors deguts a possibles efectes
de camps residuals. Per tant, comparant i representant les dades obtingudes
després de realitzar la mitjana de les mesures amb els valors teòrics obtinguts
a partir de l'equació (\ref{eq:espira}) s'obté el resultat mostrat a la figura
\ref{fig:dades_espires}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.49\textwidth]{./practica_7/grafic.pdf}
    \caption{Camp $B$ en funció del radi}
    \label{fig:dades_espires}
\end{figure}

A la figura \ref{fig:dades_espires} consisteix en la representació gràfica dels
valors obtinguts pel mòdul del camp d'inducció magnètica en funció de l'invers
del radi de cada espira. Es pot veure com realment hi ha una dependència del
camp en funció de la inversa del radi a través de la regressió lineal
realitzada.

A partir dels paràmetres de la regressió lineal es pot obtenir el valor de la
permeabilitat al buit. Sigui $B$ el pendent de la recta de regressió lineal i
$A$ l'ordenada d'origen, s'obté que els valors són de
\begin{alignat*}{2}
	&A &&= (7,36 \pm 0,10) \cdot 10^{-6} \; \unit{T} \\
	&B &&= (2,27 \pm 0,40) \cdot 10^{-6} \; \text{m} \cdot \text{T} \\
	&R^2 &&= 0,9734
\end{alignat*}

Seguidament, a partir del valor obtingut per al pendent, $B$, es pot calcular
el valor de la permeabilitat al buit amb l'equació (\ref{eq:espira_centre}). El
resultat obtingut és de $\mu_0 = (1.135 \pm 0.124) \cdot 10^{-6} \; N / A^2$,
resultat bastant proper al valor tabulat i que inclou el valor de la constant
magnètica dins del rang d'incerteses.\footnote{Tal com es pot comprovar
fàcilment a la pàgina web
\texttt{https://physics.nist.gov/cgi-bin/cuu/Value?mu0}} Tot i així, el fet
d'haver pres només tres mesures per a tres espires de radis diferents fa que
aquest resultat no es pugui considerar del tot fiable, ja que el rang de
mesures realitzades no és prou ample. Per altra banda, el valor mesurat pel
teslàmetre no és constant i, en camps tant petits com els mesurats en aquesta
part de la pràctica, la variació del valor usat per regular el teslàmetre és de
l'ordre dels valors obtinguts, amb el que no es poden donar com a resultats
precisos.

L'acumulació d'incerteses, la gran incertesa proporcionada pel teslàmetre usats
en la mesura del camp d'inducció magnètica i el baix nombre de dades preses
contribueixen a que el resultat obtingut en el càlcul de la constant magnètica
no sigui òptim, encara que aquest pugui semblar correcte.

Seguidament, per a un radi fixat, es calcula el camp d'inducció magnètica en
funció del nombre d'espires. En aquest cas el nombre d'espires calculades varia
d'un rang d'una espira fins a tres espires, i les mesures obtingudes queden
recollides a la taula \ref{tab:mesures_n_espires}. Igual que en el cas
anterior, per a reduir errors causats per camps residuals es mesura el camp al
centre de cada conjunt d'espires dues vegades; canviant el sentit del corrent
per a cada mesura. Endemés, es torna a prendre com a resultat final la mitjana
del mòdul dels valors obtinguts per al camp d'inducció magnètica.

\begin{longtblr}[theme=Personalitzat,
	label={tab:mesures_n_espires},
	caption={Mesures camp espires},
	remark{Nota}={Mesures per l'espira de radi 55 mm.},
	% note{*}={Valors en valor absolut.}
		]{
			cells={c},
			row{even}={bg=gray!5},
			row{1}={font=\bfseries},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
	Espires & $\mathbf{I}$ (\unit{A}) & $\mathbf{B}$\TblrNote{*} (\unit{mT}) \\
	\toprule
	\SetCell[r=2]{c} 1 & $4,00  \pm 0,01 $ & $0,03 \pm 0,01$ \\
	 & $-4,00 \pm 0,01 $ & $0,05 \pm 0,01$ \\
	\SetCell[r=2]{c} 2 & $4,00  \pm 0,01 $ & $0,08 \pm 0,01$ \\
	 & $-4,00 \pm 0,01 $ & $0,09 \pm 0,01$ \\
	\SetCell[r=2]{c} 3 & $4,00  \pm 0,01 $ & $0,11 \pm 0,01$ \\
	 & $-4,00 \pm 0,01 $ & $0,14 \pm 0,01$ \\
	\bottomrule
\end{longtblr}

Representant les dades de la taula \ref{tab:mesures_n_espires} obtingudes com
al camp d'inducció magnètica en funció del nombre d'espires obtenim la figura
\ref{fig:dades_n_espires}. Es pot veure que clarament el camp depèn de forma
lineal amb el nombre d'espires. Aquesta dependència lineal confirma la relació
buscada i expressada segons l'equació (\ref{eq:n_espira}).

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.49\textwidth]{./practica_7/grafic_2.pdf}
    \caption{$B$ en funció del nombre d'espires}
    \label{fig:dades_n_espires}
\end{figure}

Novament, a partir d'una regressió lineal s'estudia la dependència del camp
d'inducció magnètica amb el nombre d'espires. Sigui $B$ el pendent de la línia
de regressió, $A$ l'ordenada d'origen i $R$ el factor de relació, obtenim que
\begin{alignat*}{2}
	&A &&= (1,67 \pm 0,17) \cdot 10^{-6} \; \unit{T}\\
	&B &&= (4,25 \pm 0,31) \cdot 10^{-5} \; \unit{T}\\
	&R^2 &&= 0,999
\end{alignat*}
A partir de l'equació (\ref{eq:n_espira}) podem tornar a calcular la constant
magnètica o permeabilitat del buit, obtenint un valor de $\mu_0 = (1,254 \pm
0,104) \; N/A^2$. Aquest últim valor obtingut per a la constant magnètica és
molt més proper al valor tabulat alhora que més fiable i conté el valor de la
constant dins del rang de les incerteses.

El valor obtingut és més fiable, ja que al estar mesurant camps més grans es
redueix l'efecte de les incerteses provocades per la variació del valor mesurat
pel teslàmetre. Així doncs, els valors mesurats no es veuen greument
influenciats i dependents de la variació del valor mesurat, ja que aquest surt
de l'ordre de l'error instrumental; obtenint així un resultat més fiable. El
valor del coeficient de relació, $R$, ens mostra novament la correcció de la
hipòtesi de dependència lineal.

No obstant, tot i haver obtingut uns resultats aparentment coherents,
versemblant i bons, el nombre de mesures és bastant baix, pel que si es volgués
comprovar la relació lineal per a un cas més genèric caldria realitzar més
mesures. El problema d'això rau en la consideració de la hipòtesi del cas ideal
on, tal com s'ha mencionat en la introducció teòrica, es considera que la
distància entre les espires és menyspreable en comparació amb el radi, així que
si s'afegissin moltes espires s'hauria de deixar de considerar el cas ideal i
s'hauria de parlar d'una bobina. Justament aquest és l'objecte d'estudi a la
segona part de la pràctica.

\subsection{Mesura del camp de les bobines} \label{bobina:resultats_m}
\begin{figure*}[th]
    \centering
    \includegraphics[width=\textwidth]{./practica_7/grafic_3.pdf}
    \caption{Mesures del camp $B$ per a les quatre bobines en funció de $z$.}
    \label{fig:dades_bobines}
\end{figure*}

En aquesta segona part de la pràctica es calcula el camp d'inducció magnètica
al llarg de l'eix  de simetria de diverses bobines amb radis, nombres d'espires
i longituds diferents. Tal com s'ha comentat breument a l'apartat de
metodologia, per tal d'obtenir una quantitat significativa de dades i que
aquestes puguin ser objecte d'estudi, es prenen un major nombre de mesures als
extrems de les bobines, ja que, tal com s'ha vist a la introducció, el camp
d'inducció magnètica varia en menor quantitat al centre de les bobines.

A la taula \ref{tab:car_bobines} es mostren les característiques de les quatre
bobines estudiades; la longitud, el radi i el nombre de voltes. Les bobines
s'han anomenat $B_i$ per tal de poder comparar les seves característiques amb
els camps mesurats.

\begin{longtblr}[theme=Personalitzat,
	label={tab:car_bobines},
	caption={Característiques bobines},
		]{
			cells={c},
			row{even}={bg=gray!5},
			row{1}={font=\bfseries},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
	Bobina & $\mathbf{L}$ (\unit{cm}) & $\mathbf{R}$ (\unit{mc}) & $\mathbf{N}$ \\
	\toprule
	$B_1$ & $16,5 \pm 0.1$ & $1.65 \pm 0.05$ & 300 \\
	$B_2$ & $15,4 \pm 0.1$ & $1.30 \pm 0.05$ & 75 \\
	$B_3$ & $15,4 \pm 0.1$ & $1.30 \pm 0.05$ & 150 \\
	$B_4$ & $16,1 \pm 0.1$ & $1.30 \pm 0.05$ & 300 \\
	\bottomrule
\end{longtblr}

A la figura \ref{fig:dades_bobines} es mostren els resultats del camp
d'inducció magnètica en funció de $z$ obtinguts per a les quatre bobines
utilitzades. Aquests valors obtinguts es troben comparats amb els valors
teòrics calculats a partir de l'equació (\ref{eq:bobines}). Per a la generació
del camp mostrat s'ha fet circular una intensitat d'un ampere per les bobines.
En tots els casos, tal com s'observa a la figura \ref{fig:dades_bobines}, el
valor del camp d'inducció magnètica és màxim per a $z$ zero, és a dir, al
centre de les espires.

Tal com es pot veure a la figura, el camps d'inducció magnètica generada per la
bobina $B_4$ és aproximadament el doble que el camp generat per la bobina
$B_3$. I és que la primera té el doble de voltes que la primera i, tal com
esperàvem teòricament a partir de l'equació (\ref{eq:bobines}), els camps
generats per ambdues bobines també han de mantenir aquesta relació. El mateix
fenomen, tal com es pot veure a la taula \ref{tab:car_bobines} i la figura
\ref{fig:dades_bobines}, succeeix entre les bobines $B_3$ i $B_2$,
respectivament.

Endemés, a la figura \ref{fig:dades_bobines} també es poden estudiar tots
aquells aspectes que han sigut objecte d'estudi al llarg del tractament
d'aquesta pràctica. Per una banda, la dependència inversament proporcional
entre el camp i el radi de les bobines es pot veure a través dels resultats
obtinguts per a les bobines $B_1$ i $B_4$ ja que, tal com es pot apreciar, la
relació entre els radis i el camps generats per les bobines és inversa. Tot i
això, els resultats no acaben de mostrar-ho, ja que els valors obtinguts per a
ambdues bobines són bastant semblant i, fins i tot, en algun cas no es compleix
aquesta hipòtesi tenint en compte el rang d'incertesa.

Les incompatibilitats mostrades a la figura \ref{fig:dades_bobines} es poden
atribuir a la possible presencia de camps residuals, la imperfecció de les
bobines usades i a la manca de precisió del teslàmetre que, tal com s'ha
mencionat anteriorment, la variació de les mesures pot interferir majorment en
la determinació dels valors obtinguts per a camps petits. Per altra banda, un
possible error a tenir en compte és la correcta co\lgem ocació de la sonda dins
de la bobina, ja que és un factor determinant en la presa de dades.

No obstant, els resultats obtinguts mostren clarament el comportament esperat a
partir de les fórmules de la magnetostàtica introduïdes en la presentació
teòrica de la pràctica.

\section{Conclusions} \label{bobina:conclusio}
En la primera part d'aquesta pràctica, tal com es pretenia segons el fonament
teòric de la mateixa, s'ha constatat que el camp d'inducció magnètica al centre
del pla format per una espira manté una relació inversament proporcional al
radi de la mateixa espira. Endemés s'ha demostrat la dependència lineal entre
el camp d'inducció magnètica i el nombre d'espires, deixant en evidència la
utilitat de l'aproximació al model ideal per a un conjunt d'espires amb una
separació entre elles negligible.

S'ha pogut determinar el valor de la constant magnètica, tot i que no s'ha fet
amb total precisió per la manca d'un equip de treball amb mínimes incerteses;
per una banda, el teslàmetre presentava una gran variació en els valors
calculats en intentar calibrar-lo (moltes vegades de l'ordre dels valors que es
volien calcular) impossibilitant la correcta presa de dades i per l'altre, el
mètode emprat per trobar el centre de l'espira no assegura totalment que així
sigui.

S'ha verificat la uniformitat del camp d'inducció magnètica a l'interior d'una
bobina finita i s'ha estudiat la relació entre aquest i la geometria de
diferents bobines. L'estudi de la relació entre el camp i la geometria de les
bobines ha permès concloure en la demostració de les fórmules teòriques;
demostrant la dependència inversa entre el camp i el radi de les bobines i la
dependència lineal entre el camp i el nombre de voltes.

Per altra banda, fora dels objectius reals d'aquesta pràctica, com a material
opcional complementari a aquesta pràctica en Víctor Rodríguez ha realitzat una
simulació en tres dimensions del camp magnètic d'una bobina finita amb
\textit{python}; tal i com es suggeria al guió de la pràctica. El codi
d'aquesta simulació, a l'igual que totes les simulacions d'aquest recull
d'informes es pot trobar al final del document a la pàgina
\pageref{simulacio_4}. Tanmateix, el codi també es troba al repositori Gitlab
enllaçat.

A la figura \ref{fig:simulacio} es mostra una imatge d'aquesta simulació en
tres dimensions interactiva on es pot veure, tal com s'ha comprovat en aquesta
pràctica, que el camp magnètic és pràcticament constant a l'interior de la
bobina i pateix una gran variació als extrems de la bobina. Per altra banda, al
no considerar condicions ideals, pot haver petites distorsions del camp. Com es
pot veure al codi de la pàgina \pageref{simulacio_4}, es realitza una integral
en tot l'espai seleccionat on es calcula el camp magnètic el que fa que es
calculi el camp dins del propi fil que forma la bobina. Aquest mètode pot
causar que, al modificar les variables \texttt{densx} i \texttt{densy} (nombre
de subdivisions de l'espai on es representen les fletxes de camp) per a una
densitat majo de fletxes, apareguin linies de camp extremadament grans en
comparació a la resta de fletxes representades. És per aquesta raó que s'ha
afegit una part al codi de depuració d'errors.\footnote{Al propi codi, com el
lector podrà comprovar, s'explica que fa cada part en forma de comentaris
aclaratius.}\footnote{La imatge \ref{fig:simulacio} té una paleta de colors
diferent a la mostrada a la pàgina \pageref{simulacio_4}.}

\begin{figure}[h]
    \centering
	\includegraphics[width=0.4\textwidth, clip, trim={21cm 2cm 23cm 2cm
	0}]{./practica_7/opcional.png}
    \caption{Imatge de la simulació \texttt{\small [font:pròpia]}}
    \label{fig:simulacio}
\end{figure}

\section{Cites i Bibliografia} \label{bobina:bib}
\begin{altdescription}{[1]}
	\item[\texttt{[1]}] J. Costa Quintana y F. López Aguilar, \textit{Interacción electromagnética. Teoría clásica.} Reverté 2007.
\end{altdescription}
