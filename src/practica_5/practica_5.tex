% author: kaneda
% contingut: practica 5
% titol: mesura de la resistencia d'un metall

\title{Mesura de la resistència d'un metall}
\date{30 de març del 2023}
\maketitle

\begin{abstract}
	L'objectiu d'aquesta pràctica és la mesura experimental de la resistivitat
	d'un material metà\lgem ic pur mitjançant un sistema basat en el pont de
	Wheatstone. Endemés s'estudia la relació lineal de les mesures amb la
	temperatura a partir d'exposar el material a un ampli rang de temperatures.
	La importància de la pràctica rau en la correcta presa de dades
	experimentals per optimitzar la precisió i evidenciar la relació lineal
	entre la resistència i la temperatura del metall pur, tal com prediuen les
	hipòtesis teòriques. S'ha obtingut el valor esperat d'acord amb les
	prediccions teòriques per a la resistència del metall.
\end{abstract}

\section{Introducció i objectius} \label{metall}
L’objectiu d’aquesta pràctica és la familiarització amb tot el que comporta la
realització d’unes mesures experimentals, posant èmfasi en la determinació dels
errors que es cometen, en com optimitzar el dispositiu experimental per tal de
reduir-los al màxim i en comprovar com el comportament de les dades segueix,
dins els errors esmentats, les prediccions teòriques.

En aquesta pràctica, es mesura experimentalment la resistivitat d'un fil de
platí, un material metà\lgem ic pur, mitjançant un sistema basat en el pont de
Wheatstone.\footnote{En la introducció del fonament teòric de la pràctica
s'explicaran les diferències entre el pont de Wheatstone convencional i el que
s'usarà en la pràctica.} Es pretén confirmar la teoria, que prediu un
comportament lineal en aquest rang de temperatures, mitjançant els resultats
experimentals obtinguts.

\subsection{Fonament teòric} \label{metall:teoria}
En nombrosos conductors, com és l'exemple dels metalls a temperatura constant,
s'estableix establir una relació local molt simple entre la densitat de corrent
produïda \nta{J} i el camp elèctric \nta{E}. Aquesta relació es coneix com a la
llei d’Ohm
\begin{equation}
	\nta{J} = \sigma \nta{E} \; ,
\end{equation}
és a dir, la densitat de corrent i el camp elèctric són proporcionals. La
constant $\sigma$ es denomina conductivitat; una característica del material.
La inversa de la condictivitat és la resistivitat, $\rho$. Per altra banda, es
defineix la resistència d'un conductor, $R$, a partir de la relació
\begin{equation}
	R := \frac{V_1 - V_2}{I} \; ,
\end{equation}
on $V_1$ i $V_2$ són els valors del potencial elèctric agafats als extrems d'un
tram del conductor i $I$ és la intensitat que circula entre ells. En el cas
d'un fil conductor homogeni de secció $S$ i longitud $L$, la relació entre la
resistivitat i la resistència ve donada per
\begin{equation}
	R = \frac{\rho L}{S} \; .
\end{equation}
En el cas d'un fil prou pur de platí, que és el cas que s'estudia en aquesta
pràctica, el nombre d'impureses presents és negligible i la contribució a la
resistivitat ve fonamentalment de la interacció dels electrons amb les
excitacions de la xarxa. Aquestes excitacions s'anomenen fonons i redueixen la
mobilitat dels electrons conductors del corrent dins el metall. El fet que la
resistivitat, i per tant també la resistència, d'un metall augmenti amb la
temperatura ve explicat a partir de saber que a temperatures altes s'incrementa
el nombre de fonons. Endemés, com la probabilitat d'interaccionar dels
electrons és proporcional al nombre de fonons, que és proporcional amb la
temperatura, es manté una relació lineal.

Conseqüentment, dins del primer grau d'aproximació, la resistència dels metalls
és lineal i es pot escriure segons
\begin{equation}
	R_\theta = R_0 (1 + \beta \theta) \; ,
\end{equation}
on $\theta$ és la temperatura en graus Celsius, $R_0$ la resistència a zero
graus Celsius, $R_\theta$ la resistència a la temperatura $\theta$ i $\beta$ el
coeficient de temperatura de la resistència, és a dir, el canvi de resistència
per grau.

El dispositiu experimental del pont de Wheatstone és utilitzat per a la mesura
precisa i ràpida de resistències elèctriques. En la seva versió original està
format per quatre resistències disposades en forma de para\lgem elogram i
constituint un circuit tancat com el que es mostra a la figura
\ref{fig:pont_fil}. Els vèrtex del para\lgem elogram s'uneixen diagonalment;
una de les parelles de resistències es connecta a una font de tensió i l'altra
a un aparell per a mesurar un zero de corrent; aquest aparell pot ser un
galvanòmetre o un multímetre analògic com en el nostre cas.

El pont es considera balancejat quan, com a conseqüència del valor de les
resistències, no passa corrent pel multímetre. El dispositiu experimental
utilitzat en aquesta pràctica difereix lleugerament del típic pont de
Wheatstone. En realitat, en aquest estudi s'usa una simplificació que es coneix
pel nom de \textit{pont de fil}. Al pont de fil es substitueixen dues de les
resistències del pont de Wheatstone per un fil uniforme tensat sobre una escala
graduada, sobre la que llisca un cursor amb un contacte metà\lgem ic. El cursor
divideix la resistència total del fil en dues parts que corresponen a les
resistències substituïdes. Si es canvia la posició del cursor, la relació entre
les dues resistències substituïdes pel fil pot adquirir qualsevol valor comprès
entre zero i infinit; depenent de si el cursor es troba totalment a l'esquerra
o a la dreta, respectivament.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.49\textwidth]{./practica_5/Dibuix.pdf}
	\caption{Esquema pont Wheatstone. \texttt{\small [font:guió]}}
	\label{fig:pont_fil}
\end{figure}

Així, si el fil és homogeni i de secció uniforme, la relació entre les dues
resistències que hi ha als dos costats del cursor és la mateixa que la relació
entre les dues longituds en que queda dividit el fil segons la posició del
cursor. Quan el pont es troba balancejat, l'equació d'equilibri ve donada per
\begin{equation}
	R_1 = \frac{x}{L - x}R_2 \; , \label{eq:resistencies}
\end{equation}
on $x$ és la longitud del fil que queda a l'esquerra del cursor i $L$ és la
longitud total del fil. Per mesurar la resistència a la pràctica, doncs,
s'haurà de balancejar el pont per a cada mesura un cop s'ha aconseguit una
temperatura estable, canviant el valor de $R_2$ mitjançant una caixa de
resistències i movent el cursor, si cal, per a tenir un pas nul de corrent al
nostre mesurador en tot moment.

\section{Mètode experimental} \label{metall:metode}

En aquesta pràctica s'ha disposat d'un fil de platí pur d'un metre de longitud,
connectat a un multímetre, una caixa de resistències i a una bateria. Aquest
pont consisteix en substituir dues de les resistències del pont de Wheatstone
convencional pel fil donat, que alhora s'ha dividit en dos segments amb l'ajuda
d'un cursor mòbil. Per a poder estudiar els valors de la resistència del metall
a altes i baixes temperatures s'ha disposat d'un petit forn i d'un recipient
amb nitrogen líquid, respectivament. Durant el transcurs de la pràctica s'han
pres mesures de les temperatures per a ambdues parts de la pràctica; a baixes
temperatures i a altes temperatures.

Per simplificar els càlculs, durant la pràctica es manté el cursor a una
distància de $L/2$ per tal que l'expressió (\ref{eq:resistencies})  quedi
simplificada segons
\begin{equation}
    R_1 = R_2 \; .
\end{equation}
Endemés, a l'hora de fer les preses de la temperatura del material metà\lgem ic
sempre es mira que el valor del corrent que passa pel multímetre sigui nul, tal
com s'ha explicat a la part teòrica. Així mateix, per poder fer-ho s'ha
modificat regularment la resistència del circuit amb la caixa de resistències
mencionada i de la que s'han extret els valors experimentals.

Endemés, per poder treballar un rang més extens de temperatures, s'ha dividit
l'estudi d'aquesta pràctica en dues parts; l'estudi de la dependència de la
resistència a temperatures baixes i per a temperatures altes.

En primer lloc s'ha mesurat el voltatge subministrat per la bateria, tot
tancant el circuit amb l'interruptor Morse, que ha permès controlar el circuit.
Una vegada mesurat aquest valor s'ha procedit amb la mesura del valor de la
resistència a diferents temperatures, començant per un escalfament del material
amb el forn. Abans d'encendre el forn es mesuren les condicions inicials; la
temperatura ambient i la resistència inicial.

S'han anat prenent mesures simultànies de la resistència i la temperatura del
metall mentre augmentava la temperatura del forn fins a una temperatura final
d'uns tres-cents graus Celsius. Endemés s'ha comprovat en tot moment que el
corrent que passava pel multímetre del circuit era nul per poder seguir
considerant les simplificacions contemplades.

Anàlogament, per a la mesura de la resistència a temperatures baixes s'ha
desconnectat el forn i s'ha deixat el material metà\lgem ic refredant fins a
temperatura ambient. Seguidament, amb el nitrogen líquid s'ha refredat el
material fins a una temperatura propera als cent graus Celsius negatius. A
l'haver assolit una temperatura constant ha començat la presa de dades de la
resistència i la temperatura seguint la metodologia emprada a la part anterior
fins a arribar a la temperatura ambient mesurada en iniciar la pràctica.

\subsection{Presa de dades}

Com s'ha comentat anteriorment, el mètode triat per a la mesura de les dades
juga un rol força important en aquesta pràctica. Ja que, sense una bona
estratègia, les incerteses podrien ser molt elevades i podrien perjudicar així
els resultats obtinguts. Amb aquesta finalitat, l'estratègia seguida a la
pràctica ha estat fixar el valor de la resistència $R_1$, i posteriorment
esperar que la diferència de potencial al pont sigui zero. En aquell moment
s'anota el valor de la temperatura marcat pel termòmetre, reduint així el
possible error humà al mínim.

\section{Resultats i discussió} \label{metall:resultats}

Primerament han sigut mesurades les condicions inicials de l'experiment; per
una banda es determina la temperatura ambient del laboratori i la resistència
del material metà\lgem ic a aquesta temperatura. Aquestes mesures donen els
valors de
\begin{alignat*}{1}
	T &= (23,7 \pm 0,1) \, ^\circ C \\
	R &= (111,1 \pm 0,1) \, \Omega
\end{alignat*}
respectivament. Les incerteses venen donades per les incerteses instrumentals
del multímetre i del termòmetre amb que s'han realitzat les mesures. Un cop
realitzat aquest pas previ, tal com s'ha explicat a la introducció, es
procedeix a mesurar la temperatura i la resistència del material per a un ampli
rang de temperatures usant el forn i el nitrogen líquid. Els resultats
d'aquestes mesures es poden veure a la figura \ref{fig:5:regressio}, on s'ha
expressat la resistència del material en funció de la temperatura.

\begin{figure}[h]
    \centering
	\includegraphics[width=0.49\textwidth]{./practica_5/grafic.pdf}
	\caption{Resistència en funció de la temperatura}
	\label{fig:5:regressio}
\end{figure}
Tal com es pot veure a la figura \ref{fig:5:regressio}, els valors que
s'obtenen encaixen a la perfecció amb les hipòtesis teòriques. Es pot apreciar
clarament com realment hi ha una dependència lineal entre la temperatura del
material i la resistència del mateix. Endemés, el coeficient de determinació,
$R^2$, ha sigut de 0,998 que ens confirma la concordança amb les hipòtesis
teòriques. A la figura \ref{fig:5:regressio} s'han volgut marcar les condicions
inicials de l'experiment, ja que els valors superiors a aquest punt s'han
obtingut a partir de l'escalfament del material amb el forn i els punts
inferiors s'han obtingut a partir del refredament del material amb el nitrogen
líquid.

No obstant, també es pot apreciar com, tot i encaixar amb la regressió lineal,
hi ha alguns valors que difereixen de la dependència lineal. Aquests poden
haver sigut donats per imperfeccions del material negligides, ja que s'ha
considerat que es treballava amb un material perfectament pur. També són
notables les desviacions que s'han obtingut per a temperatures molt baixes on,
tal com es pot veure a la figura, la resistència és molt menor a la que
s'esperaria d'una dependència lineal. Els errors per a temperatures baixes
poden haver sigut donats per el mètode usat en la mesura de les dades en aquest
interval de temperatures i en la manipulació del muntatge.

Les dades obtingudes han pogut permetre mesurar el valor del canvi de
resistència per grau; el paràmetre $\beta$. El valor obtingut a partir del
pendent ha sigut de $(0,00444 \pm 0,00019) ^\circ C^{-1}$, que és un resultat
molt proper als valors tabulats.\footnote{Veure font \texttt{[2]} a l'apartat
\textit{Cites i Biliografia}.}

El fet d'haver connectat el pont de fil amb un cursor a una distància fixada i
haver mesurat la temperatura a mesura que s'anava mesurant la resistència del
material ha permès minimitzar els possibles errors sistemàtics i reduir les
valoracions d'incertesa. Per tant, el fet d'haver seguit aquest mètode
experimental ha permès obtenir uns resultats molt bons i molt propers als que
s'esperaven teòricament.

\section{Conclusions} \label{metall:conclusio}
Els resultats obtinguts en la realització d'aquesta pràctica encaixen a la
perfecció amb les prediccions teòriques. Endemés, l'estudi de les dades
obtingudes a partir de la regressió mostra que, efectivament, hi ha una clara
dependència lineal entre la temperatura i la resistència del fil de platí pur
usat. No obstant, s'hauria de trobar una explicació més adequada per a la
variació i discrepància dels valors de resistència obtinguts per a temperatures
molt baixes.

Per poder comprovar realment que aquest error ve donat únicament per una mala
praxis a l'hora de prendre mesures amb el nitrogen líquid a temperatures baixes
caldria repetir la presa de dades per aquestes temperatures properes a cent
graus Celsius negatius. No obstant, a la figura \ref{fig:5:regressio} es pot
veure com les altres dades mesurades per a temperatures inferiors a les
inicials sí que es troben dins del rang d'aproximació teòrica.

De l'estudi de les dades a partir de la regressió lineal es pot concloure que
les dades obtingudes són molt properes a la relació lineal esperada per les
hipòtesis teòriques, ja que el coeficient de determinació és pràcticament la
unitat. Endemés, tal com s'esperava per a un material metà\lgem ic com el platí,
es pot veure que el coeficient de resistència amb la temperatura és
pràcticament constant amb la temperatura i del qual es té una incertesa molt
baixa. Alhora, el valor obtingut per aquest coeficient és molt proper al valor
tabulat per al platí.

Per tant, es pot concloure que l'estudi de la resistència del fil de platí pur
en funció de la temperatura ha permès confirmar les hipòtesis teòriques
formulades. Els resultats experimentals mostren clarament la dependència lineal
entre la resistència del material i la temperatura, tal com es volia comprovar
i s'ha pogut calcular el coeficient de resistència del platí amb la
temperatura.

\section{Cites i Bibliografia} \label{metall:biblio}
\begin{altdescription}{[1]}
	\item[\texttt{[1]}] C. Kittel, \textit{Introducción a la Física del Estado Sólido}, ed. Reverté.
	\item[\texttt{[2]}] Olmo, M. i Nave, R. \textit{Resistividad y coeficiente
		de temperatura a 20 $^\circ$ C}, hyperphysics.
\end{altdescription}
