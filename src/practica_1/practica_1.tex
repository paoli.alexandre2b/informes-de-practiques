% author: kaneda
% contingut: practica 1
% titol: representacio de camps

\title{Representació de camps}
\date{11 de maig del 2023}
\maketitle

\begin{abstract}
	En aquesta pràctica s'aprofita la dualitat existent entre la densitat de
	corrent, $\nta{J}$, i el vector desplaçament, $\nta{D}$, per representar
	experimentalment les línies equipotencials i de camp d'un condensador pla,
	de dos fils carregats i d'un conductor amb geometria regular arbitrària.
	Cada configuració es dibuixa sobre un paper conductor fent ús d'un
	retolador de plata per representar els elèctrodes i s'hi aplica una font de
	corrent mentre es mesura el potencial amb un multímetre. Finalment, es
	calcula la capacitat per unitat de longitud de cada configuració.
\end{abstract}

\section{Introducció i objectius} \label{c}
Al llarg d'aquesta pràctica experimental es duu a terme la representació de les
línies equipotencials i de les línies de camp elèctric, que són perpendiculars
a les línies equipotencials traçades, en tres situacions electrostàtiques
concretes. En primer lloc, s'estudien dues plaques d'un condensador
plano-para\lgem el de llargada infinita. Seguidament, dos fils para\lgem els
també infinits i, en darrer lloc, una figura similar a un prisma infinit de
base quadrada dins el qual s'hi troben dues plaques connectades
perpendicularment entre si en forma de lletra `ela'.\footnote{Veure figura
\ref{fig:simulacio_lliure} de l'apartat de resultats per a una representació de
la configuració descrita.} En efecte, cadascuna de les configuracions es
representa sobre una superfície plana, el que permet reduir el problema a dues
dimensions.

Tal i com s'ha anticipat, l'objectiu principal és aconseguir traçar diverses
línies equipotencials, principalment tancades, al paper de carbó en el que s'hi
han representat les configuracions descrites. Per tal de dur a terme aquest
objectiu es realitza un traçat de punts notablement propers entre ells
resseguint un mateix valor de potencial calculat. Més endavant és detallarà
quins són els passos concrets que cal seguir per poder realitzar la pràctica
experimental.

Pel que fa a les línies de camp elèctric, per teoria es sap que aquestes són
perpendiculars a les línies equipotencials, atès que el camp elèctric és igual
al negatiu del gradient de potencial. Tanmateix, també es sap que les línies
del camp sempre van en sentit de disminució del potencial.

En darrera instància, en aquesta pràctica es calcula experimentalment la
capacitat per unitat de longitud del condensador aproximant el càlcul integral
a un sumatori de petites variacions de potencial, $\Delta V$, i de distàncies
$\Delta l$ i $\Delta r$. Per efectuar un estudi més acurat de les situacions
presentades també es comparen els resultats obtinguts amb les prediccions
teòriques.

\subsection{Dualitat entre $\nta{J}$ i $\nta{D}$} \label{c:dualitat}
La densitat de corrent, $\nta{J}$, i el vector desplaçament, $\nta{D}$,
presenten unes relacions duals com a conseqüència d'estar, en molts materials,
linealment relacionats amb el camp elèctric, $\nta{E}$. A una regió amb camps
conservatius s'apliquen les següents equacions per materials isòtrops, lineals
i homogenis
\begin{gather*}
    \boldsymbol{\nabla} \times \nta{E} = 0 \; ,\\
    \nta{J} = \sigma \nta{E} \; ,\\
    \boldsymbol{\nabla} \cdot \nta{J} = 0 \; ,
\end{gather*}
si es tracta d'un medi conductor i, si el medi és dielèctric s'apliquen les
equacions següents
\begin{gather*}
    \boldsymbol{\nabla} \times \nta{E} = 0 \; , \\
    \nta{D} = \epsilon \nta{E} \; , \\
    \boldsymbol{\nabla} \cdot \nta{D} = 0 \; .
\end{gather*}

En ambdós casos, en tractar-se els medis lineals i homogenis, els paràmetres
$\epsilon$ i $\sigma$ són constants i es compleix que el rotacional de la
densitat de corrent i del vector desplaçament són nuls. Aquest fet ens indica
que tant la densitat de corrent com el vector desplaçament poden derivar-se
d'un potencial escalar; que anomenarem $U$ i $U'$, respectivament. En ambdós
casos es complirà la coneguda com equació de Laplace
\begin{gather}
    \boldsymbol{\nabla}^2 U = 0 \\
    \boldsymbol{\nabla}^2 U' = 0
\end{gather}
Per trobar una solució per a la densitat de corrent i per al vector
desplaçament només cal trobar dos potencials escalars, $U$ i $U'$, que
satisfacin les equacions de Laplace i les condicions de contorn imposades en
cada cas.

A partir de les expressions mostrades és fàcil veure que qualsevol solució per
a la densitat de corrent pot ser solució per al vector desplaçament,
intercanviant les magnituds $\nta{J}$ per $\nta{D}$ i $\sigma$ per $\epsilon$.
Així doncs, si es coneix la solució a un problema determinat en un medi
conductor, aquesta serà la solució del problema corresponent o equivalent a un
medi dielèctric sempre i quant les condicions de contorn siguin equivalents i
no s'hi trobin discontinuïtats de $\sigma$ i $\nta{E}$ al medi.

\subsection{Càlcul de la capacitat d'un condensador} \label{c:calcul}
Es considera una superfície equipotencial que tanca una de les plaques del
condensador. El flux del vector desplaçament està relacionat amb la càrrega
total de la placa, segons el teorema de Gauss
\begin{equation}
    Q = \epsilon \int_S \nta{E} \cdot \nta{dS},
\end{equation}
Considerant que el camp elèctric es manté constant amb l'alçada $Z$ del
condensador, que es considera molt gran, i que el diferencial de superfície es
defineix com $dS=Zdl$ on $dl$ és el diferencial de longitud a la intersecció de
la superfície equipotencial amb un pla perpendicular al condensador, tal i com
es troba a la primera configuració experimental, s'obté que la càrrega total de
la placa del condensador ve descrita segons
\begin{equation}
    Q = \epsilon Z \oint Edl,
\end{equation}
Fàcilment es pot veure que la càrrega per unitat de longitud, $Z$, de la placa
del condensador és
\begin{equation}
	\frac{Q}{Z} = \epsilon \oint E dl, \label{eq:carrega/Z}
\end{equation}
Tal com s'ha explicat a la introducció teòrica, per poder calcular la integral
anterior es pren una de les línies equipotencials trobades experimentalment i
s'aproxima la integral pel sumatori dels increments de longitud de la línia
equipotencial escollida per el camp elèctric en cada un d'aquests increments.
El camp elèctric per a cada increment de longitud es calcula com
\begin{equation}
	E_i = \frac{\Delta V_i}{\Delta r_i}, \label{eq:camp_diferencials}
\end{equation}
on $\Delta r_i$ és l'increment en la distància radial i $\Delta V_i$ és la
diferència de potencial a l'increment de longitud de la línia
equipotencial.\footnote{A la pràctica, la diferència de potencial és la
diferència de potencial calculat entre dues de les línies equipotencials
traçades.} Així doncs, partint de les consideracions anteriors i de les
equacions (\ref{eq:carrega/Z}) i (\ref{eq:camp_diferencials}), s'obté que la
càrrega per unitat de longitud del condensador es pot aproximar com
\begin{equation}
    \frac{Q}{Z} \simeq \epsilon \sum_i \frac{\Delta V_i \Delta l_i}{\Delta r_i}.
    \label{eq:Q/Z_exp}
\end{equation}
Seguidament, a partir del resultat anterior, es pot trobar la capacitat del
condensador per unitat de longitud de la placa
\begin{equation}
    \frac{C}{Z} = \frac{Q/Z}{\Delta_{ab}},
    \label{eq:C/Z_exp}
\end{equation}
on $\Delta_{ab}$ és la diferència de potencial existent entre les dues plaques
del condensador.

\section{Mètode experimental} \label{c:metode}
Per tal de representar les línies equipotencials i realitzar les mesures de les
tres configuracions diferents es dibuixen les figures corresponents en uns
fulls de paper impregnats amb carbó de conductivitat homogènia que seràn usats
com a medi conductor. El paper usat presenta un rang de resistències d'entre
$5$ i $20$ k$\Omega$ per quadrat. La representació de les figures mencionades
es realitza a partir de les seccions transversals de cadascuna d'elles atès que
es treballa sobre una superfície bidimensional i es vol representar un tall o
secció de l'objecte tridimensional.

Sobre aquests fulls es representaran les adients figures usant un retolador que
allibera tinta conductora a base de partícules de plata en suspensió en un
líquid. Passat un cert temps, d'aproximadament vint minuts, el líquid queda
solidificat i s'obté la màxima conductivitat. Es situa el paper sobre d'un suro
i, amb xinxetes, es fixen els seus extrems per tal que el full romangui immòbil
al llarg del procés. Aleshores, es connecten els elèctrodes dibuixats a una
font de corrent DC mitjançant uns cables que queden fixats punxant una xinxeta,
tal i com mostra la figura \ref{fig:electrodexinxeta}.
\begin{figure}[ht]
    \centering
	\includegraphics[width=0.49\textwidth]{./practica_1/muntatge.pdf}
    \caption{Connexions de l'elèctrode \texttt{\small[font:guió]}}
    \label{fig:electrodexinxeta}
\end{figure}

En afegit, un dels dos elèctrodes es connecta a un multímetre per tal d'obtenir
una referència de potencial. Per consegüent, l'altre extrem del multímetre
permet mesurar la diferència de potencial a qualsevol punt del paper,
senzillament, tocant amb la punxa el punt que es pretengui.

Finalment, per dibuixar una línia equipotencial, s'escull un valor determinat
de voltatge i es va prosseguint una línia de punts amb el mateix valor en
qüestió, marcant-la suaument amb un llapis. Aquests punts marcats son units per
tal de representar les línies equipotencials. En aquesta pràctica es tracen més
de dues línies equipotencials per contiguració per tal de tenir més material
d'estudi.

A partir de les línies equipotencials anteriors es pot dibuixar les línies de
camp elèctric, que resulten de traces perpendiculars a les rectes tangents a
les línies equipotencials.

En últim terme, tal com s'ha introduït anteriorment, es calcula la capacitat
per unitat de longitud del condensador a partir de les equacions presentades.
Endemés, per poder comparar els resultats obtinguts amb els teòrics es realitza
una simulació en \textit{python} de les tres configuracions. A les simulacions
es presenten tant les línies equipotencials com una representació del camp
elèctric. El codi usat per a fer les representacions es pot trobar a l'apartat
de \hyperlink{page.42}{\textit{Codis de les simulacions}} així com al
repositori de Gitlab.

\section{Resultats i discussió} \label{c:resultats}
En aquest apartat es pretén estudiar de manera més detallada els resultats
corresponents a les tres configuracions realitzades. Més precisament,
s'analitza específicament com són les línies equipotencials així com les línies
de camp i s'estudien els valors obtinguts per al condensador plano-para\lgem
el, els dos fils infinits i la configuració lliure.

En afegit, les tres situacions electrostàtiques també han estat simulades per
ordinador per tal de poder comparar-les amb les dades experimentals i
extreure'n conclusions. Tanmateix, es calcula la capacitat del condensador per
unitat de longitud, d'una banda, de manera teòrica i, d'altra banda, en relació
amb els resultats corresponents a les mesures. Finalment, s'analitzen els
valors trobats per veure si es corresponen.

\subsection{Condensador plano-para\lgem el infinit} \label{c:res:cond}
Tal i com s'ha descrit anteriorment, aquesta configuració electrostàtica
representa la secció transversal d'un condensador amb dues plaques
plano-para\lgem eles de llargada infinita amb una diferència de
potencial entre les plaques de $(10,0 \pm 0,1)$ V.

D'una banda, pel que fa a les línies equipotencials, observant la figura
\ref{fig:equipotencial_condensador} és clar que aquestes són molt similars al
que s'esperava, en termes generals. En efecte, cap d'elles es creua i cadascuna
d'elles es tanca amb una simetria prou contundent. Aprofundint, els valors de
potencial escollits per a la representació han estat els de les línies amb
valors de $(9,5 \pm 0,1)$ V, $(8,5 \pm 0,1)$ V i de $(5,0 \pm 0,1)$ V per a la
línia tancada més propera al condensador dret, la línia amb forma similar però
més ample i la línia pràcticament recta totalment centrada entre les dues
plaques, respectivament. Cal destacar que han estat utilitzats valors anàlegs i
simètrics de potencial per les línies corresponents a la placa de l'esquerra de
$(0,5 \pm 0,1)$ V i $(1,5 \pm 0,1)$ V. En afegit, la figura
\ref{fig:simulacio_condensador} mostra la simulació de \textit{python} de les
línies en qüestió per aquesta configuració concreta.\footnote{El codi de les
simulacions es troba al final de l'informe i al repositori de Gitlab.}

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.49\textwidth, clip, trim={2.5cm 15cm 0 2cm 0}]{./practica_1/condensador_2.pdf}
    \caption{Resultats condensador}
    \label{fig:equipotencial_condensador}
\end{figure}

D'altra banda, tal com s'ha avançat anteriorment, a la mateixa simulació de
\textit{python} també si han representat les línies de camp elèctric com es pot
veure a la figura \ref{fig:simulacio_condensador}. S'aprecia que aquestes són
totalment perpendiculars a les línies de potencial constant i sempre van en
sentit de la disminució d'aquest. Per a una bona representació de les línies de
camp, de fet, només cal veure que aquestes siguin totalment perpendiculars a
les rectes tangents a les corbes equipotencials calculades, seguint el
procediment per al dibuix manual de les línies de camp.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.49\textwidth, clip, trim={2.3cm 0.2cm 2.3cm 0.2cm
    0}]{./practica_1/condensador.pdf}
    \caption{Simulació condensador}
    \label{fig:simulacio_condensador}
\end{figure}

Un cop mostrades ambues figures, la figura \ref{fig:equipotencial_condensador}
i la figura \ref{fig:simulacio_condensador}, es pot observar com ambdues
figures mostren que les línies equipotencials a l'interior de les plaques
presenten una certa curvatura, a diferècnia de les prediccions teòriques per a
un condensador plano-para\lgem el idealment infinit; i aquí rau la qüestió, no
s'està tractant amb un condensador infinit. La consideració d'un condensador
infinit només és vàlida per aquelles configuracions on la distància entre les
plaques és notablement inferior a la longitud d'aquestes; condició que no es
compleix en aquest experiment. Aquest fet, alhora, explica el perquè d'unes
línies de camp no del tot perpendiculars a les plaques del condensador al seu
interior. En ambdues imatges, encara que petita, les línies de camp presenten
una curvatura a l'interior.

Altrament, comparant novament les figures \ref{fig:equipotencial_condensador} i
\ref{fig:simulacio_condensador} es pot apreciar també una alteració o
deformació en l'eix perpendicular a les plaques en les línies equipotencials;
aquestes últimes poden semblar axatades. Aquesta deformació s'atribueix al
mètode de mesura i és que el fet d'aplicar certa pressió sobre el paper en que
s'han près les mesures pot alterar el valor obtingut. Si s'aplica una pressió
no constant sobre el paper pot variar el valor de la resistència en aquella
zona i, per tant, mesurar un valor diferent de potencial.

Malgrat aquestes diferències entre les condicions presentades i un condensador
plano-para\lgem el ideal infinit es pot veure com els resultats experimentals
s'aproximen a la simulació realitzada per a les condicions de laboratori i
concorden amb les prediccions per a un condensador no infinit.

A continuació, es pretén calcular la capacitat del condensador, és a dir, la
capacitat per unitat de longitud dels elèctrodes para\lgem els a la primera
situació electrostàtica. D'una banda, per calcular la capacitat de manera
teòrica es fa ús de la següent expressió
\begin{equation}
    \frac{C}{Z} = \epsilon \frac{L}{d} \; ,
\end{equation}
on $L$ denota l'amplada dels elèctrodes del condensador, $\epsilon$ la
permitivitat del material i $d$ la distància entre les pròpies plaques. A la
figura \ref{fig:equipotencial_condensador} es pot apreciar quins són els valors
corresponents a $L$ i $d$. Ergo, el valor de la capacitat és ($1,33 \pm 0,43 $)
$\epsilon$ F/m.

D'altra banda, anteriorment s'ha detallat quins són els passos a seguir per a
obtenir el valor de la capacitat del condensador mitjançant les mesures
experimentals realitzades. Per tant, es comença calculant la càrrega per unitat
de longitud fent ús de l'equació (\ref{eq:Q/Z_exp}) juntament amb les dades
proporcionades a la taula \ref{tab:mesures_condensador} de
l'apèndix.\footnote{Veure la taula \ref{tab:mesures_condensador} a la pàgina
\pageref{tab:mesures_condensador} de l'annex.} El valor obtingut és de ($20,9
\pm 5,4$)\;$\epsilon$~C/m. Seguidament, mitjançant l'equació (\ref{eq:C/Z_exp})
i emprant el valor de $(10,0 \pm 0,1)$~V de diferència de potencial entre els
elèctrodes es troba el valor final experimental de la capacitat per unitat de
longitud del condensador. En efecte, aquest és de ($2,09 \pm 0,11
$)\;$\epsilon$ F/m. Així doncs, calculats els dos valors desitjats, és possible
obtenir una certa estimació de l'error de càlcul. Precisament, s'hi troba un
defecte aproximat del $36\%$.

Amb l'objectiu de comparar ambdós resultats, és possible extreure'n un seguit
d'afirmacions. En primera instància, resulta evident que els valors obtinguts
no són compatibles entre si. En segon lloc, l'error de càlcul obtingut és força
elevat.

Per tant, cal evidenciar que, tal i com s'explica en la introducció, les
expressions utilitzades per calcular el valor experimental de la capacitat
s'obtenen en base a una aproximació del Teorema de Gauss en forma integral. Per
consegüent, el resultat obtingut s'interpreta com una aproximació relacionada
amb un nombre finit de mesures i no pas com un valor real i exacte. En adició,
és probable que influeixi una manca d'exactitud i precisió a l'hora de prendre
les mesures dels diversos diferencials corresponents a la taula
\ref{tab:mesures_condensador}. Finalment, també seria possible que el dibuix de
les plaques del condensador no sigui totalment simètric i correcte, és a dir,
que els dos elèctrodes no siguin exactament iguals i presentin quelcom
desperfecte.

\subsection{Fils para\lgem els infinits} \label{c:res:fil}
La segona situació electrostàtica està formada per dos fils para\lgem els de
llargada infinita. En efecte, es tracta també d'una secció transversal
d'aquests i la diferència de potencial donada entre els fils és de $(10,0 \pm
0,1)$~V. Com des de la teoria es sap que les línies equipotencials per a la
distribució estudiada són cercles de diferents radis i amb origens que es van
allunyant de l'eix de simetria a mesura que agmenta el radi, es pretenen
mesurar diverses línies equipotencials tancades. Aquestes mesures serviran per
estudiar i determinar la forma de les corves equipotencials experimentals en
relació amb les teòriques.

D'entrada, a la figura \ref{fig:equipotencial_fils} s'aprecia que les línies
equipotencials experimentals obtingudes en aquesta configuració també
s'assemblen molt al que es preveia. Els valors de potencial escollits han estat
de $(1,0 \pm 0,1)$ V i $(9,0 \pm 0,1)$ V per les línies tancades més properes
als fils, de $(2,0 \pm 0,1)$ V i $(8,0 \pm 0,1)$~V per les segones línies
tancades, $(3,5 \pm 0,1)$ V i $(6,5 \pm 0,1)$ V per les línies no tancades i
$(5,0 \pm 0,1)$ V per a la línia coincident amb l'eix de simetria de la
configuració. Noti's, altra vegada, que es fa us de potencials anàlegs i
simètrics per ambdós fils.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.49\textwidth, clip, trim={1cm 16cm 0.5cm 1cm
    0}]{./practica_1/fils.pdf}
    \caption{Resultats fils infinits}
    \label{fig:equipotencial_fils}
\end{figure}

Seguidament, la figura \ref{fig:simulacio_fils} representa una simulació tant
de les línies equipotencials com del camp elèctric corresponent, tal com s'ha
mostrat en la configuració anterior. Dit això, és clar que hi ha gran similitud
amb la representació experimental i es corresponen amb el que s'espera de
manera teòrica. És a dir, les línies són perpendiculars entre si i el camp va
en sentit de disminució del potencial. No obstant, novament és notable una
deformació en l'eix perpendicular a l'eix de simetria de la configuració per
aquelles línies equipotencials més distants als fils. L'explicació d'aquesta
anomalia experimental podria ser, igual que en l'apartat anterior, deguda a la
metodologia duta a terme en les mesures.

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.49\textwidth]{./practica_1/fils_infinits.pdf}
    \caption{Simulació fils infinits}
    \label{fig:simulacio_fils}
\end{figure}

Per altra banda, es pot observar com les línies equipotencials experimentals
més properes als fils, mostrades a la figura \ref{fig:equipotencial_fils}, sí
que s'aproximen notablement a les línies mostrades a la simulació de la figura
\ref{fig:simulacio_fils}, fet que sembla reforçar la hipòtesi d'una possible
variació dels resultats per la pràctica en el mètode de mesura.

\subsection{Prisma quadrat infinit amb plaques cèntriques perpendiculars en L} \label{c:res:ll}
En darrer lloc, s'ha treballat amb una configuració formada per un prisma de
base quadrada i llargada infinita dins el qual s'hi troben dues plaques
connectades perpendicularment amb forma de lletra `ela'. La diferència de
potencial entre el prisma i les plaques internes és, novament, de $(10,0 \pm
0,1)$~V.

De manera similar a les dues distribucions anteriors, en relació amb les línies
equipotencials obtingudes experimentalment representades a la figura
\ref{fig:equipotencial_lliure}, de nou, es pot dir que aquestes són molt
semblants al que s'esperava teòricament. Per aquesta configuració els valors de
potencial escollits han estat de $(1,0 \pm 0,1)$~V per la línia tancada més
propera a les plaques internes, de $(4,0 \pm 0,1)$~V per la mitjana i de $(6,0
\pm 0,1)$~V per la més llunyana, respectivament. A més, cal notar que fora del
prisma el multímetre sempre marca zero vols atès que el prisma és tancat i, per
consegüent, no hi ha diferència de potencial entre l'elèctrode i qualsevol punt
exterior.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.49\textwidth, clip, trim={2.5cm 15cm 0 2cm
    0}]{./practica_1/lliure.pdf}
    \caption{Resultats figura lliure}
    \label{fig:equipotencial_lliure}
\end{figure}

A la figura \ref{fig:simulacio_lliure} es mostra una simulació feta per
ordinador de la configuració lliure estudiada, que permet veure com són ambdues
línies. Tal i com s'esperava, la representació concorda molt notablement amb
les línies experimentals i el camp elèctric és perpendicular i en sentit de
disminució del potencial. En aquest cas, es pot veure com la representació de
les línies equipotencials a la figura \ref{fig:equipotencial_lliure} no
presenten cap deformació notable respecte la simulació de \textit{python} de la
figura \ref{fig:simulacio_lliure}. Aquest fet pot haver sigut per una major
precisió en les mesures, per una millor metodologia o per la pròpia distribució
estudiada. Tal com es pot veure, és la única de les tres distribucions
estudiades que no presenta deformació en l'eix perpendicular a l'eix de
simetria.

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.49\textwidth]{./practica_1/fig_lliure.pdf}
    \caption{Simulació figura lliure}
    \label{fig:simulacio_lliure}
\end{figure}

\section{Conclusions} \label{c:conclusio}
Inicialment, es destaca que s'han obtingut representacions de les línies
equipotencials així com del camp elèctric per a cadascuna de les tres
configuracions estudiades. En relació amb aquestes, els resultats mesurats
s'aproximen amb gran similitud al que s'esperava i es preveia de manera
teòrica. En particular, no es troba cap discrepància en el traçat de les línies
més que unes petites alteracions en el sentit perpendicular a l'eix de simetria
de les dues primeres configuracions. Per consegüent, aquest fet ha permès
certificar experimentalment que les línies equipotencials en les situacions
electrostàtiques treballades mai es tallen i presenten una simetria coherent. A
més, es confirma que les línies de camp elèctric són totalment en direcció
perpendicular i sentit de disminució del potencial.

Seguidament, les simulacions programades per ordinador són compatibles amb
totalitat amb les figures obtingudes mitjançant les mesures experimentals.
Ergo, altra vegada es confirma que els resultats són notablement satisfactoris
i coherents amb el que prediu la teoria electrostàtica.

Finalment, en quant als valors de les capacitats del condensador de plaques
para\lgem eles, el resultat obtingut experimentalment no és compatible amb el
valor calculat de manera teòrica, el que ens indica que no resulta possible
aproximar el condensador dibuixat a un condensador ideal infinit.

\section{Cites i Bibliografia}
\begin{altdescription}{[1]}
	\item[\texttt{[1]}] Ángel J. García Collado y José A. Ruiz Templado, \textit{Principios de electrostática.} Garcia editores 2004.
\end{altdescription}
