% author: kaneda
% contingut: practica 6
% titol: feixos de raigs catodics

\title{Feixos de raigs catòdics}
\date{27 d'abril del 2023}
\maketitle

\begin{abstract}
	En aquesta pràctica s'analitzen i s'estudien el que es coneix com a feixos
	de raigs catòdics. Endemés, es demostra que els feixos de raigs catòdics
	estan formats per unes partícules amb una relació entre la càrrega i la
	massa igual a la relació que presenten els electrons; concloent així que
	estan formats per electrons. Aquesta comprovació es duu a terme mitjançant
	l'anàlisi de les trajectòries dels feixos de partícules provocades per la
	interacció dels feixos amb camps magnètics i elèctrics controlats; produïts
	amb l'ajuda de dues bobines i un condensador, respectivament.
\end{abstract}

\section{Introducció i objectius} \label{feixos}
El principal objectiu d'aquesta pràctica és trobar la relació entre la càrrega
i la massa de les partícules que componen els raigs catòdics i verificar que
aquesta relació és igual a la relació entre càrrega i massa de l'electró.
Endemés, com a objectiu secundari interessa la comprovació experimental de
l'expressió de Lorentz per a la força electromagnètica
\begin{equation}
  \nta{F}=q(\nta{E}+\nta{v}\wedge\nta{B}) \; .
\end{equation}

Per assolir aquests objectius es generen camps elèctrics i magnètics de
diferent intensitat que travessen perpendicularment la trajectòria del feix de
partícules desconegudes i s'analitzen curosament les conseqüències que aquests
camps provoquen en les seves trajectòries. En primer terme s'estudien les
desviacions del feix de partícules en condicions d'electrostàtica a través de
la generació d'un camp uniforme amb un condensador. En segon terme s'estudia en
condicions de magnetostàtica i, finalment, es combinen els dos camps; elèctric
i magnètic, per a trobar la relació entre la càrrega i la massa de les
partícules del feix i demostrar així que els feixos de raigs catòdics estan
formats per electrons. Així doncs, es pot considerar que la pràctica es
divideix en tres parts: la desviació electrostàtica, la desviació
magnetostàtica i la desviació electromagnètica.

\subsection{Desviació electrostàtica} \label{feixos:desv_e}
Inicialment, en la realització de la primera part de la pràctica, s'aplica un
diferència de potencial constant controlat, $V_p$, entre les plaques d'un
condensador situat a l'interior del tub de raigs catòdics. Aquest voltatge
genera un camp elèctric entre les plaques del condensador, la magnitud del qual
es pot expressar a partir de
\begin{equation}
    E=\frac{kV_p}{d} \; , \label{eq:campelectric}
\end{equation}
on $d$ és la distància entre les plaques del condensador i $k$ és una constant
que té en compte els efectes de vorada. L'ànode accelera els raigs catòdics i
els focalitza en un feix rectangular, sigui $V_a$ el potencial per accelerar
les partícules aplicat en l'eix $x$, per la conservació de l'energia s'obté que
\begin{equation}
    \frac{1}{2}mv_o^2=qV_a \; ,\label{eq:conservacioE}
\end{equation}
on $m$ i $q$ són les respectives massa i càrrega de les partícules del feix i
$v_o$ és la seva velocitat. Seguidament, utilitzant les fórmules del moviment
rectilini uniforme i del moviment rectilini uniformement accelerat i notant
que, per Newton, l'acceleració que rebran les partícules és $a=-qE/m$, sabem
que, al llarg del temps, les partícules seguiran la trajectòria descrita segons
\begin{equation}
      y=-\frac{qEx^2}{2mv_0^2}
\end{equation}
i, per tant, si es substitueixen les expressions (\ref{eq:campelectric}) i
(\ref{eq:conservacioE}) a aquesta última expressió, es pot veure que el feix de
partícules carregades segueix la paràbola descrita per la trajectòria
\begin{equation}
    y=-\frac{kV_px^2}{4dV_a} \; . \label{eq:electroparabola}
\end{equation}
La comprovació de la validesa de la fórmula anterior és també un dels objectius
d'aquesta secció de la pràctica.

\subsection{Desviació magnetostàtica} \label{feixos:desv_m}
En l'estudi de la desviació magnetostàtica, s'utilitzaran dues bobines
Helmholtz,\footnote{En el cas d'aquesta pràctica, es bobines Helmholtz estan
fetes de fil de coure esmaltat bobinat a un suport de plàstic.} una a cada
banda del tub de raig catòdics, per a la producció del camp magnètic. La funció
d'aquestes bobines és la creació d'un camp magnètic pràcticament uniforme a la
regió central de les bobines i, en la geometria en la que es duu a terme la
pràctica, aquest camp ve descrit segons l'expressió
\begin{equation}
	\nta{B}=\frac{32\pi nI}{5\sqrt{5}r} \cdot 10^{-7}  \;\nta{e}_z \;
	\unit{Wb.m^{-2}} \; , \label{eq:valorB}
\end{equation}
on $n$ és el nombre de voltes de les bobines, $r$ és el radi de les bobines i
la distància a la que es troben separades i $I$ la intensitat subministrada. En
la pràctica la direcció de $\nta{B}$ és perpendicular a la direcció que segueix
el feix, provocant que les partícules quedin restringides a una trajectòria
circular. És important remarcar que les dues bobines es troben separades a una
distància $r$; igual al seu radi. Conegut aquest fet i aplicant l'expressió de
la força de Lorentz es pot veure fàcilment el requeriment que s'haurà de
complir
\begin{equation}
    Bq=\frac{mv}{R} \; ,\label{eq:BqmvR}
\end{equation}
on $m$ és la massa de les partícules, $v$ és la velocitat que porten les
partícules, $q$ és la càrrega de les partícules i el radi de la trajectòria,
$R$, ve descrit segons l'expressió
\begin{equation}
	R=\frac{x^2+y^2}{2y} \; . \label{eq:calculradi}
\end{equation}
Finalment, si es suposa que la velocitat de les partícules, per conservació de
l'energia, ve donada per l'expressió (\ref{eq:conservacioE}) es pot obtenir
fàcilment que el quocient entre la càrrega i la massa de les partícules ve
descrit per l'equació
\begin{equation}
    \frac{q}{m}=\frac{2V_a}{B^2R^2} \label{eq:q/m mgneto}
\end{equation}
on $V_a$ és el potencial d'acceleració subministrat a l'ànode i $R$ és el radi
de les trajectòries mesurat experimentalment.

\subsection{Desviació electromagnètica} \label{feixos:desv_em}
Si un camp elèctric $\nta{E}$ i un camp magnètic $\nta{B}$ perpendiculars entre
si, interaccionen amb una partícula que es mou ortogonalment a ambdós camps
tindrem que, per Newton, les forces elèctrica i magnètica s'igualen. Sigui $q$
la càrrega de la partícula i $v$ la velocitat, aïllant la velocitat de la
igualtat entre les forces produïdes en aquesta situació s'obté
\begin{equation}
    v=\frac{E}{B} \; . \label{eq:develectro_v}
\end{equation}
A continuació, si es suprimeix el camp elèctric $\nta{E}$ subministrat i es
mesura el radi de la trajectòria circular que segueix el feix de partícules;
agafant l'equació (\ref{eq:BqmvR}), el valor del radi d'aquesta trajectòria
vindrà donat per
\begin{equation}
    R=\frac{mv}{qB} \; . \label{eq: radi2.3}
\end{equation}
En ajuntar les equacions (\ref{eq:develectro_v}) i (\ref{eq: radi2.3}) es pot
deduir trivialment que la relació entre la càrrega i la massa de les partícules
que composen els raigs catòdics ve donada segons
\begin{equation}
    \label{eq: q/m}
    \frac{q}{m}=\frac{E}{RB^2}=\frac{kV_p}{dK^2I^2R}
\end{equation}
on $K$ és la constant de les bobines de Helmholtz, $R$ és el radi de la
trajectòria circular i $k$ és la constant del condensador.

\section{Mètode experimental} \label{feixos:metode}
Una vegada vistos els objectius a assolir i les qüestions a les quals se cerca
resposta, és necessari presentar la metodologia amb la que es duen a terme els
experiments per tal de demostrar les hipòtesis teòriques plantejades
prèviament.

Primerament, es munta el circuit de la figura \ref{fig:circuitelectro} on
s'utilitza una bombeta de vidre a la qual se li ha fet el buit a l'interior i
un condensador amb una separació entre les plaques metà\lgem iques de 54
\unit{mm} situat dins de la mateixa bombeta. Una vegada comprovat el correcte
muntatge del circuit pel professor de laboratori corresponent, s'aplica una
diferència de potencial de 4 \unit{kV} a l'ànode i es comprova que el feix de
partícules no mostra cap mena de desviació.

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.49\textwidth]{./practica_6/Circuitelectro.png}
    \caption{Muntatge primera part \texttt{\small[font:guió]}}
    \label{fig:circuitelectro}
\end{figure}
Seguidament, mantenint constant el potencial de l'ànode, es subministra una
diferència de potencial de 3 kV entre les plaques del condensador i es
realitzen fotografies adients de la trajectòria seguida pel feix de partícules.
A continuació, es repeteix el mateix procediment amb diferents valors per a la
diferència de potencial, $V_p$, concretament s'aplica un voltatge de $1,2,3$ i
$4$ kV i es realitza novament la corresponent presa de dades. L'anàlisi
d'aquestes mesures permet comprovar posteriorment la validesa de l'equació
(\ref{eq:electroparabola}) i calcular els diferents valors de $k$ a partir de
l'estudi de la regressió lineal.

Per a procedir amb la segona part de la pràctica es desconnecten les plaques
del condensador per tal de suprimir el camp elèctric i es realitza el muntatge
de la figura \ref{fig:circuitmagneto}.

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.49\textwidth]{./practica_6/Circuitmagneto.png}
    \caption{Muntatge segona part \texttt{\small[font:guió]}}
    \label{fig:circuitmagneto}
\end{figure}

Una vegada realitzat i comprovat el correcte muntatge, s'aplica un potencial
d'acceleració a l'ànode amb valor de $3$ kV. Seguidament es subministren
diferents valors d'intensitat a les bobines i es prenen les mesures
corresponents per estudiar i precisar la trajectòria del feix de partícules en
relació amb la intensitat subministrada; concretament es prenen set mesures
diferents, variant la intensitat subministrada entre $100$ \unit{mA} i $800$
\unit{mA}, extraient les dades de les trajectòries en un interval de $100$ mA.

Seguidament, amb intenció de trobar la relació entre la càrrega i la massa de
les partícules i, emprant l'equació (\ref{eq:q/m mgneto}), es fixa una
intensitat de $300$ mA a les bobines Helmholtz i es subministren diferents
valors de potencial a l'ànode. Es prenen les dades corresponents pel rang de
valors de potencial de $2$ kV fins a un valor de $5$ kV.

Finalment, pel que respecta a la desviació electromagnètica, s'apliquen
simultàniament un camp elèctric, $\nta{E}$, i un camp magnètic, $\nta{B}$,
ortogonals entre si seguint el circuit representat a la figura
\ref{fig:circuitmagneto}. Així mateix, també es connecta un generador de
potencial entre les plaques del conductor com apareix a la figura
\ref{fig:circuitelectro}. Per a fer-ho s'aplica un voltatge a l'ànode de $3$ kV
i una intensitat de $100$ mA fixats, mentre es varia el potencial $V_p$ entre
les plaques fins a observar una nu\lgem a desviació del feix.

Una vegada obtinguda la situació desitjada es suprimeix el camp elèctric,
$\nta{E}$, i es mesura el radi de la trajectòria; resultant únicament de la
desviació magnètica. Amb les dades obtingudes s'empra l'equació (\ref{eq: q/m})
per a obtenir la relació entre la càrrega i la massa de les partícules del
feix.

\subsection{Tractament de les dades}
Pel que respecta al tractament de les dades, en aquesta pràctica s'han
realitzat fotografies de les trajectòries dels feixos per a cada apartat i per
a cada variació; tant de la diferència de potencial subministrat com de la
intensitat administrada a les bobines. Posteriorment, s'han editat aquestes
fotografies per poder extreure la informació desitjada amb la màxima facilitat
i rigurositat possible i s'han analitzat pacientment per a obtenir les dades de
les trajectòries dels feixos. En la redacció d'aquest informe no s'adjunten les
imatges experimentals, però algunes de les imatges analitzades i de les imatges
experimentals es poden trobar a l'annex de la pàgina
\pageref{app:feixos:fotos}.\footnote{Totes les imatges experimentals es poden
trobar al repositori de Gitlab, incloent aquelles que no es mostren en aquest
informe.}

\section{Resultats i discussió} \label{feixos:resultats}
En aquest apartat es presenten, comenten i discuteixen els resultats obtinguts
en la realització de la pràctica seguint la consideració de separar el
tractament de les dades en tres seccions, tal com s'ha fet a la introducció
teòrica.\footnote{En la presentació de resultats es denota el radi amb la $r$
per no confondre'l amb el coeficient de relació de les regressions lineals,
denotat per $R$.}

\subsection{Desviació electrostàtica} \label{feixos:res_e}
Una vegada construït el circuit corresponent a la figura
\ref{fig:circuitelectro} i comprovada per la professora de laboratori la
correcta realització del muntatge, s'ha aplicat un potencial de l'ànode de
valor $(4 \pm 0.2)$~kV. Tal i com s'esperava, en no existir cap camp
perpendicular a la trajectòria del feix, s'ha obtingut una desviació nu\lgem a
d'aquest.

Feta aquesta comprovació, s'ha obtingut que, en aplicar una diferència de
potencial de $(3 \pm 0.2)$~kV entre les plaques, els raigs, efectivament, es
desvien seguint una trajectòria corba. Vista aquesta trajectòria i preses les
fotografies corresponents, s'ha repetit el procés aplicant una diferència de
potencial de $V_p=(1,2,3,4 \pm 0.2)$~kV. Per a totes les fotografies preses
s'han realitzat les adients mesures. La següent gràfica \ref{fig:graficelectro}
creada a partir de les dades obtingudes, mostra la relació entre $x^2$ i $y$ on
$x$ i $y$ són les coordenades de les trajectòries del feix de partícules per
als diferents valors del potencial $V_p$ aplicat entre les plaques.

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.49\textwidth]{./practica_6/grafic_1.pdf}
	\caption{Valors de $y$ en funció de $x^2$ segons $V_p$}
    \label{fig:graficelectro}
\end{figure}

Amb les dades mostrades a la figura \ref{fig:graficelectro} es pot comprovar si
la trajectòria del feix segueix una expressió de l'estil $y=Ax^2+B$. A la taula
\ref{tab:regressiopolelectro} es mostren els paràmetres obtinguts en fer
l'ajust lineal amb les dades de la figura
\ref{fig:graficelectro}.\footnote{Veure la taula \ref{tab:regressiopolelectro}
de la pàgina següent.}

\begin{table*}
	\centering
	\caption{Aproximació polinomial per als diferents valors de $V_p$}
	\label{tab:regressiopolelectro}
	\begin{tblr}
			{
			cells={c},
			row{even}={bg=gray!5},
			row{1}={font=\bfseries},
			rowhead=1
		}
		\toprule
		$V_p$ (kV) & $A$ & $B$ & $R$ & $k$ \\
		\toprule
		1 & $1,033 \pm 0,009$ & $(-2,81 \pm 0,49) \cdot 10^{-4}$ &
		0,999 & $0,66 \pm 0,14$ \\
		2 & $2,057 \pm 0,043$ & $(-1,31 \pm 0,23) \cdot 10^{-3}$ &
		0,997 & $0,66 \pm 0,14$ \\
		3 & $4,13 \pm 0,14$   & $(-2,58 \pm 0,50) \cdot 10^{-3}$ &
		0,994 & $0,89 \pm 0,18$ \\
		4 & $7,07 \pm 0,42$   & $(-2,92 \pm 0,91) \cdot 10^{-3}$ &
		0,989 & $1,15 \pm 0,23$ \\
		\bottomrule
	\end{tblr}
\end{table*}

Noti's que, degut a que el valor de $B$ és pràcticament zero per a tots els
valors de $V_p$ es dedueix que, tal i com suggeria l'equació
(\ref{eq:electroparabola}), el feix segueix una trajectòria descrita per
l'equació $y=Ax^2$ on $A=kV_p/4dV_a$. Aquesta última expressió permet calcular
també, tal i com interessava, el valor de la constant $k$ del condensador en
cada cas, que s'ha afegit a l'última columna de la taula
\ref{tab:regressiopolelectro}. Es pot veure com el valor $k$ augmenta a mesura
que s'incrementa el potencial aplicat $V_p$.

\subsection{Bobines de Helmholtz i desviació magnetostàtica} \label{feixos:res_m}
S'estudiarà ara la trajectòria descrita per les partícules dels raigs en
aplicar diferents intensitats a les Bobines de Helmholtz generant-se així un
camp magnètic. L'objectiu presentat per aquesta part de la pràctica és
comprovar experimentalment que les partícules estudiades segueixen una
trajectòria circular de radi $r$, calculat a partir de l'equació
(\ref{eq:calculradi}).

A la gràfica \ref{fig:grafici} es mostren els resultats obtinguts en aplicar un
potencial a l'ànode de $(3.0\pm 0.2)$~\unit{kV}. Per altra banda, a la taula
\ref{tab:regressioIR} es mostren els corresponents valors de $r$ calculats
mitjançant la regressió lineal de les magnituds $2y$ i $x^2+y^2$ representades
a la figura \ref{fig:grafici}.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.49\textwidth]{./practica_6/grafic_2.pdf}
    \caption{Valors de $x^2+y^2$ en funció de $2y$}
    \label{fig:grafici}
\end{figure}

De la taula \ref{tab:regressioIR} i la figura \ref{fig:grafici} podem extreure
principalment dues conclusions dignes de menció. Primerament, notem que els
valors de $R^2$ de la regressió lineal s'aproximen en gran mesura a 1, el seu
valor màxim. Això indica que les trajectòries descrites pels feixos catòdics
són efectivament circulars i el seu radi ve descrit per l'equació
(\ref{eq:calculradi}). A més a més, veiem que efectivament el radi, $r$, de les
trajectòries disminueix a mesura que la intensitat aplicada a les bobines $I$
augmenta, tal com s'esperava. Aquesta dependència en $I $ es pot entendre com a
conseqüència de la força de Lorentz que experimenten les partícules, ja que
d'aquí es veu que $r\propto \nta{B}$ i com s'ha vist a l'equació
(\ref{eq:valorB}), $\nta{B} \propto I$ i, per tant $r \propto I$. Veiem doncs
que la relació que s'esperaria entre els radis hauria de ser lineal amb $I$, en
el cas estudiat no s'aprecia aquesta relació lineal amb tota l'exactitud
esperada.  Aquesta discrepància amb la relació esperada pot ser deguda a
l'acumulació d'errors en la mesura de les trajectòries del feix a partir de les
fotografies preses a classe i al fet que el gruix d'alguns feixos no ha permès
definir amb extrema claredat els valors $x$ i $y$ de les trajectòries per alguns
valors d'intensitat.

\begin{longtblr}[theme=Personalitzat,
	label={tab:regressioIR},
	caption={Aproximació lineal dels valors de $r$ respecte a $I$},
	remark{Nota}={Mesures amb un potencial d'acceleració a l'ànode de $300$
	\unit{kV}.},
	note{*}={Valors sense incertesa.}
		]{
			cells={c},
			row{even}={bg=gray!5},
			row{1}={font=\bfseries},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
	$I$\TblrNote{*} (\unit{mA}) & $r$ (\unit{cm}) & $R^2$ \\
	\toprule
	100 & $30,68 \pm 0,58$ & 0,999 \\
    200 & $20,40 \pm 0,74$ & 0,998 \\
    300 & $13,26 \pm 0,27$ & 0,997 \\
    400 & $11,23 \pm 0,33$ & 0,996 \\
    500 & $8,49 \pm 0,34$ & 0,996 \\
    600 & $6,64 \pm 0,44$ & 0,999 \\
    700 & $6,33 \pm 0,35$ & 0,993 \\
    800 & $5,07 \pm 0,16$ & 0,999 \\
	\bottomrule
\end{longtblr}

En aquest apartat es demanava repetir altra vegada el mateix procés, mantenint
ara un valor fix de $I = (300 \pm 20)$ \unit{mA} i variant el valor de la
diferència de potencial $V_a$ subministrada en l'ànode. Les relacions entre els
valors $2y$ i $x^2+y^2$ de les trajectòries en funció del potencial aplicat es
troben en la figura \ref{fig:graficfixe} i el valor dels corresponents radis
$r$ calculats mitjançant una regressió lineal de les dades s'exposen en la
taula \ref{tab:regressiofixe}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.49\textwidth]{./practica_6/grafic_3.pdf}
    \caption{Valors de $x^2+y^2$ en funció de $2y$}
    \label{fig:graficfixe}
\end{figure}

Igual que en cas anterior, si un mira la taula \ref{tab:regressiofixe} nota que
els valors obtinguts pel coeficient $R^2$ de la regressió lineal són tots força
propers al seu màxim valor. Això torna a confirmar que, efectivament, les
partícules segueixen una trajectòria circular. En aquest cas podem explicar la
relació entre $r$ i $V_a$ mitjançant les equacions (\ref{eq:BqmvR}) i
(\ref{eq:conservacioE}). De la primera es dedueix que $r \propto v$ i de la
segona que $v \propto \sqrt{V_a}$. Així doncs, s'esperaria trobar una relació
$r \propto \sqrt{V_a}$ i, com es pot calcular a partir de les dades de la
\ref{tab:regressiofixe} ara si, s'ha obtingut el resultat esperat.

D'aquests dos subapartats s'ha calculat, tal com es demanava, la relació entre
la càrrega i la massa de les partícules a partir de l'equació (\ref{eq:q/m
mgneto}) en cada cas, on el valor del camp $\nta{B}$ s'ha calculat mitjançant
l'equació (\ref{eq:valorB}). Fent la mitjana d'aquestes relacions s'obté un
valor de la relació càrrega massa de $q/m=(2.05\pm 0.78) \cdot 10^{11}$
\unit{C/kg}. Aquest valor coincideix exitosament en l'orde de magnitud amb la
relació entre la càrrega i la massa dels electrons i, a més, l'exacte valor
d'aquesta magnitud entra dins el rang d'incertesa dels resultats experimentals.

\begin{longtblr}[theme=Personalitzat,
	label={tab:regressiofixe},
	caption={Aproximació lineal dels valors de $R$ respecta a $I$},
	remark{Nota}={Mesures amb una intensitat de $300$ \unit{mA}.},
	note{*}={Valors sense incertesa.}
		]{
			cells={c},
			row{even}={bg=gray!5},
			row{1}={font=\bfseries},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
	$V_p$\TblrNote{*} (\unit{kV}) & $r$ (\unit{cm}) & $R^2$ \\
	\toprule
	200 & $11,06 \pm 0,58$ & 0,999 \\
    300 & $12,86 \pm 0,14$ & 0,999 \\
    400 & $15,40 \pm 0,27$ & 0,998 \\
    500 & $16,92 \pm 0,20$ & 0,999 \\
	\bottomrule
\end{longtblr}

\subsection{Desviació electromagnètica}
En aquest apartat, seguint l'exposat a la secció \textit{Mètode experimental},
s'ha aplicat una intensitat $I = (100 \pm 20)$ \unit{mA} a les bobines i una
diferència de potencial $V_a = (3\pm 0.2)$ \unit{kV} a l'ànode. S'ha vist que el
potencial necessari a aplicar entre les plaques del condensador per a
visualitzar una nul\lgem a desviació del feix té un valor de $V_p =
(0.9\pm0.2)$ \unit{kV}.  En suprimir el camp elèctric, $\nta{E}$, notem que
altra vegada el feix segueix una trajectòria corba, concretament una circular
de radi de $r = (33.96\pm 0.75)$ \unit{cm} calculat a partir de l'equació
(\ref{eq:calculradi}). Ara, a partir de l'equació (\ref{eq: q/m}), trobem que
la relació càrrega massa de les partícules constituent dels raigs catòdics és
$q/m=(1.83\pm 0.42)\cdot 10^{11}$ \unit{C/kg}. Per aquest càlcul s'ha utilitzat
el valor de la constant del condensador $k = (0.66\pm 0.14)$ obtinguda a
l'apartat d'electrostàtica. Si es comparen els dos valors obtinguts per aquesta
relació càrrega-massa es veu que ambdós són força semblants entre ells, i tot i
no ser exactament iguals que el valor esperat ($e/m_e=1.76\cdot 10^{11}$), són
força propers i el valor teòric esperat entra dins del rang d'incertesa dels
dos valors experimentals.\footnote{Veure font \texttt{[1]} de la secció
\textit{Cites i Bibliografia}.} Es dedueix d'aquí que les partícules que
componen els raigs catòdics podrien ser electrons degut a la similitud de la
seva relació entre la càrrega i la massa.

\section{Conclusions} \label{feixos:conclusio}
Primerament, dels apartats d'electrostàtica i magnetostàtica s'ha pogut
comprovar experimentalment que les partícules del feix seguien una trajectòria
parabòlica i circular en aplicar respectius camps elèctric, $\nta{E}$, i
magnètic, $\nta{B}$.  Observem que les dades preses s'aproximen en gran mesura
a les trajectòries esmentades, obtenint així valors $R^2$ de regressió lineal
sempre majors a $R^2=0.989$.

S'ha observat també que es compleixen les dependències d'aquestes trajectòries
respecte als valors característics del circuit i les diferències de potencial i
les intensitats aplicades. En altres paraules, s'ha comprovat que es compleixen
de manera satisfactòria les equacions teòriques que es pretenia posar a prova.
Comentar també que únicament no hem obtingut aquesta dependència esperada en el
cas de la magnetostàtica, ja que els resultats obtinguts no mostraven de manera
clara una relació inversament proporcional entre el radi de les trajectòries i
intensitats aplicades a les bobines. Possiblement aquesta discrepància es degui
a errors acumulats en la presa de dades i al fet que, en el cas de
magnetostàtica, el gruix d'alguns feixos no permetia establir amb tota claredat
la posició exacta de les partícules.

Finalment, pel que respecta a la relació entre la càrrega i la massa de les
partícules que componen els feixos de raigs catòdics, s'han obtingut resultats
molt congruents amb els esperats. Notem que els dos valors obtinguts no
discrepen gaire entre ells i, en els dos casos, el valor esperat entre dins del
rang d'incertesa dels experimentals. Es dedueix d'aquí que les partícules que
componen el feix tenen una relació entre la càrrega i la massa força similar a
la dels electrons, donant la sospita que, tal com sabem, en efecte, són
electrons.

\section{Cites i Bibliografia}
\begin{altdescription}{[1]}
	\item[\texttt{[1]}] Fundamental Physical Constants, \textit{Electron charge
		to mass quotient}, NIST Database.
		\url{https://physics.nist.gov/cgi-bin/cuu/Value?esme}
\end{altdescription}
