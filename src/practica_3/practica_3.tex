% contingut: practica 3
% titol: circuit RLC en serie

\begin{abstract}
	En aquesta pràctica s'analitza el comportament d'un circuit $RLC$ depenent
	del tipus de senyal subministrat. Primerament, utilitzant un senyal
	sinusoïdal, l'estudi es centra principalment en la ressonància en el règim
	permanent, juntament amb els seus paràmetres característics. En segon lloc,
	a través d'un senyal rectangular s'estudien els tres possibles escenaris
	del règim transitori; sense osci\lgem acions, amb osci\lgem acions
	amortides i amb osci\lgem acions crítiques. Encara que tots els resultats
	obtinguts són compatibles amb les prediccions teòriques, cal destacar que
	el valor obtingut per a la resistència crítica i el guany de la resistència
	petita, disten bastant del valor esperat teòricament a l'haver considerat
	un comportament ideal dels components del circuit i haver negligit les
	respectives resistències internes.
\end{abstract}

\paragraph*{Descripció intuïtiva:} \label{RLC}
Un circuit que consta d'una bobina sense resistència òhmica i un condensador
carregat connectats, produeix un procés d'osci\lgem ació en el qual el
condensador es descarrega a través de la bobina i es torna a carregar, generant
un voltatge osci\lgem ant indefinidament. Això és degut a l'oposició que la
força electromotriu induïda de la bobina ofereix al canvi de corrent, segons la
llei de Lenz. La resistència òhmica nu\lgem a de la bobina evita que circuli
una intensitat infinita, i la intensitat osci\lgem a a mesura que el
condensador es carrega i descarrega. Aquesta osci\lgem ació es pot visualitzar
a través d'un osci\lgem oscopi i té aplicacions pràctiques en altres circuits.

Si disminuïm la capacitat $C$ del condensador o la inductància $L$ de la
bobina, la descàrrega del condensador es produeix més ràpidament ja que té
menys càrrega a emmagatzemar i hi haurà una major variació en la intensitat per
a una mateixa força electromotriu induïda. Aquest fenòmen provoca una
disminució del temps d'osci\lgem ació i un augment de la freqüència d'osci\lgem
ació. Si la resistència òhmica no és nu\lgem a, la bobina perdrà energia en
forma de calor en cada osci\lgem ació, i el potencial en el condensador
disminuirà cada vegada més fins que arribi a zero. Si la resistència és prou
gran, l'osci\lgem ació cessarà.

\section{Introducció i objectius} \label{RLC:intro}
Un circuit $RLC$ és un circuit elèctric format per la combinació d'una
resistència elèctrica $R$, una bobina $L$ i un condensador $C$. Aquests
circuits són d'especial interès ja que es tracta de la classe de circuits més
elemental pels quals el voltatge osci\lgem a i, a més, presenten com a objecte
d'estudi les diferents maneres d'osci\lgem ació en relació a les
característiques del circuit. Aquesta pràctica es centra principalment en
l'estudi de dos règims diferents caracteritzats pel tipus de voltatge que es
subministra al circuit $RLC$; el règim permanent sinusoïdal i el règim
transitori.

Els objectius principals de la pràctica són la familiarització amb el circuit
$RLC$ en sèrie, ja que al ser el circuit més elemental que presenta osci\lgem
acions és de gran interès pràctic, i la visualització i comprensió de les
diferents solucions de l'equació diferencial de segon ordre pròpia d'aquest
circuit a través de l'oci\lgem oscopi. Endemés es comprova el comportament i
les magnituds característiques d'aquest circuit, en relació amb el tipus de
voltatge aplicat i el valor de la resistència emprada. En el règim permanent,
el principal objectiu és determinar la freqüència de ressonància i estudiar les
freqüències de tall del circuit, amb l'afegit de veure com varia el valor
numèric d'aquestes magnituds en canviar la resistència del circuit. Tanmateix,
es qüestiona la validesa de les prediccions teòriques per a resistències
relativament petites.

En quant al règim transitori sinusoïdal, l'objectiu primordial és caracteritzar
el comportament del voltatge del circuit al llarg del temps, segons el valor de
la resistència i identificar els tres tipus de solucions de l'equació
diferencial; amortida, crítica i aperiòdica o sense osci\lgem ació.

\subsection{Fonament teòric} \label{RLC:teoria}
Com s'ha comentat anteriorment, el comportament d'aquest circuit $RLC$ es pot
descriure mitjançant una equació diferencial de segon ordre. En aquesta
pràctica s'estudia el circuit $RLC$ en sèrie, on la tensió total del circuit és
la suma de les tensions en cada component, és a dir, el voltatge total del
circuit és
\begin{equation}
	V = V_R + V_L + V_C = RI + L\frac{dI}{dt} + \frac{1}{C} \int I dt \; ,
	\label{eq:voltatge}
\end{equation}
on $V_R$ és el voltatge de la resistència, $V_L$ el de la bobina, $V_C$ el del
condensador i $I$ és la intensitat del corrent. Les osci\lgem acions del
circuit són originades per un generador de senyals per observar certs fenòmens,
com ara la ressonància. Segons el tipus de senyal d'entrada proporcionat al
sistema, rectangular o sinusoïdal, es distingirà entre règim transitori o règim
permanent.

Al règim permanent sinusoïdal, com el seu propi nom indica, s'aplica un
voltatge d'entrada que segueix una funció sinusoïdal. Conseqüentment, la
solució de l'equació (\ref{eq:voltatge}) és també una intensitat sinusoïdal.
Fent servir notació complexa i prenent la intensitat com a origen, es té que
els valors del voltatge i el corrent al llarg del temps venen donats,
respectivament, per
\begin{alignat}{1}
	V(t)&=V_{max}e^{j(\omega t+\phi)} \\ I(t)&=I_{max}e^{j(\omega t)}
\end{alignat}
on $\omega$ és la pseudo-pulsació o pseudo-freqüència. Substituint els valors
del voltatge i la intensitat de corrent a l'equació (\ref{eq:voltatge}) s'obté
\begin{equation}
	V(t) = \left[R + j\left(L\omega - \frac{1}{C\omega}\right)\right] I(t) = Z
	I(t) \; \label{eq:vregimP}
\end{equation}
on s'ha definit la impedància del circuit, $Z$. D'aquesta última equació es pot
veure directament que quan els efectes de la bobina i el condensador es
compensen, de manera que la tensió i la intensitat estan en fase, té lloc el
fenomen de la ressonància; per a un cert voltatge la intensitat del circuit és
màxima mentre el terme complexe de la impedància és nul. És a dir, quan la
freqüència $\omega_0$ a la que osci\lgem a el voltatge subministrat compleix
\begin{equation}
	\omega_0^2 = \frac{1}{LC} \; ,\label{eq:omega0}
\end{equation}
on es defineix $\omega_0$ com la freqüència pròpia o de ressonància del circuit
$RLC$. El guany en voltatge a un element del circuit es defineix com la relació
entre el voltatge a aquest element i el voltatge d'entrada. Així, el guany en
voltatge a la resistència ve donat per
\begin{equation}
	T_R = \frac{V_R}{V_E} = \frac{R}{R + j (L\omega - \frac{1}{C\omega})} \; .
	\label{eq:guany}
\end{equation}
Si animenem $\phi$ al desfasament entre els voltatges $V_E$ i $V_R$, com a
conseqüència d'aquesta última expressió (\ref{eq:guany}), si la freqüència del
circuit, $\omega$, dista inferiorment de la freqüència de ressonància,
$\omega_0$, el desplaçament $\phi$ és positiu i, per tant, el voltatge a la
resistència anirà avançant respecte al voltatge d'entrada. Anàlogament, si la
freqüència del circuit és superior a la freqüència de ressonància ocorrerà el
cas contrari. Noti's que l'equació (\ref{eq:guany}) mostra que en la freqüència
de ressonància el guany és màxim; el guany és la unitat.

Dues magnituds interessants en l'estudi de l'amplada de la ressonància són les
anomenades freqüències de tall, $\omega_1$ i $\omega_2$. Aquestes es defineixen
com les freqüències d'osci\lgem ació per a les quals el guany és $1/\sqrt{2}$.
Per tant, substituïnt el valor del guany a l'equació (\ref{eq:guany}) s'obté
que les freqüències de tall són aquelles que compleixen
\begin{alignat}{1}
	\frac{\omega_2-\omega_1}{\omega_0}&=\frac{R}{L\omega_0}\\
	\omega_2\cdot\omega_1&=\omega_0^2
\end{alignat}
Noti's finalment que l'amplada de banda entre les freqüències de tall
$\omega_1$ i $ \omega_2$ és
\begin{equation}
	\Delta \omega = \omega_2 - \omega_1 = \frac{R}{L} \; . \label{eq:1}
\end{equation}

Respecte al règim transitori, es subministra un senyal rectangular. Si es resol
l'equació diferencial (\ref{eq:voltatge}) del circuit es troba que el sistema
té diferents solucions o maneres d'osci\lgem ar en relació amb el valor, ja
sigui positiu, negatiu o igual a zero, de la magnitud
\begin{equation}
	\Delta = \left(\frac{R}{L}\right)^2 - \frac{4}{LC} \; . \label{eq:delta}
\end{equation}
Així doncs, per a uns valors d'inductància i capacitància donats, es tindran
tres comportaments diferents depenent del valor de l'equació (\ref{eq:delta})
en funció de la resistència del circuit.

Quan el discriminant és negatiu es donen osci\lgem acions amortides l'amplitud
de les quals decau exponencialment seguint una constant d'esmortiment $\lambda$
i amb un freqüència de pseudo-pulsació definida com
\begin{equation}
	\omega=\sqrt{\omega_0^2-\left(\frac{R}{2L}\right)^2} \; .
\end{equation}

Per consegüent, noti's que el pseudo-període d'oscilació ve donat per
\begin{equation}
	T = \frac{4 \pi}{\sqrt{\frac{4}{LC} - \left(\frac{R}{L}\right)^2}} \; .
\end{equation}
Així doncs, noti's que quan la resistència és mínima, és a dir, quan la
resistència és aproximadament nu\lgem a, es té que la freqüència d'oci\lgem
ació és aproximadament la freqüència de ressonància.

Quan el discriminant definit a l'expressió (\ref{eq:delta}) és zero es dona la
situació d'osci\lgem ació críticament amortida. És fàcil demostrar que s'obté
aquest tipus d'osci\lgem ació quan la resistència compleix
\begin{equation}
	R_C=2\omega_0L=2\sqrt{\frac{L}{C}} \label{eq:resist_critica}
\end{equation}

Finalment, quan el discriminant és positiu, es dona la situació d'osci\lgem
ació sobreamortida, el comportament de la qual és una caiguda del corrent
transitori sense que es produeixin osci\lgem acions.

\section{Mètode experimental} \label{RLC:metode}
En la primera part de la pràctica, en el règim permanent, s'utilitzen dues
resistències: una de $2700 \Omega$ i l'altra de $270 \Omega$. A través de
l'osci\lgem oscopi, són observades les osci\lgem acions que depenen de la
freqüència d'entrada subministrada i de la resistència utilitzada. També es
determina la freqüència de ressonància.

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.45\textwidth, trim={0 8.9cm 0 0 0}, clip]{./practica_3/imatges/circuits.pdf}
    \caption{Circuit règim permanent. \texttt{\small[font:guió]}}
    \label{fig:circuit1}
\end{figure}

La primera part de la pràctica parteix del muntatge de l'esquema elèctric del
circuit $RLC$, el qual inclou la bobina, $L$, el condensador, $C$, i la
resistència, $R$, de 22mH, 15nF i 2700$\Omega$ respectivament, tal com es
mostra a la figura \ref{fig:circuit1}. Es realitza la connexió de dos punts del
circuit als dos canals de l'osci\lgem oscopi, amb la finalitat de visualitzar i
comparar els senyals.\footnote{Un dels senyals es mostra a la pantalla de
l'oci\lgem oscopi de color groc, mentre que l'altre és blau, tal com es pot
veure a les imatges de l'apartat \textit{Material fotogràfic}.} Una connexió es
situa abans de la bobina per observar el senyal d'entrada, mentre que una altra
es co\lgem oca entre el condensador i la resistència per visualitzar la
variació del senyal. Així, un canal de l'osci\lgem oscopi mesura $V_E$ i
l'altre mesura $V_R$. Seguidament, es mesuren la freqüència de ressonància i
les freqüències de tall per la resistència inicial. Per trobar la freqüència de
ressonància, s'ajusta la freqüència d'entrada de l'ona sinusoïdal amb el
generador d'ones mentre es visualitzen els senyals de $V_E$ i $V_R$ a la
pantalla de l'osci\lgem oscopi, fins que es detecta que ambdós senyals estan en
fase. Així mateix, es mesuren les freqüències de tall observant les diferents
amplituds i fent un anàlisi dels senyals a l'osci\lgem oscopi.

A la segona part de la pràctica, s'estudia el comportament transitori del
circuit $RLC$ i es pretén comprovar experimentalment l'amortiment del senyal en
funció de la resistència en relació amb la resistència crítica. Així doncs, per
a dur a terme aquesta part experimental es fa ús d'un generador d'ones
quadrades, a diferència del generador de corrent altern utilitzat a la part
anterior. En aquest cas, per a la segona part, es parteix del muntatge de
l'esquema del circuit mostrat a la figura \ref{fig:circuit2}.

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.45\textwidth, trim={0 0 0 9.2cm 0}, clip]{./practica_3/imatges/circuits.pdf}
    \caption{Circuit règim transitori. \texttt{\small[font:guió]}}
    \label{fig:circuit2}
\end{figure}

Tal i com es pot apreciar, la bobina d'inductància $L$, la resistència $R$ i el
condensador $C$ també formen part d'aquest segon circuit, de valors 33mH, 330pF
i 180$\Omega$ respectivament. No obstant, amb una disposició lleugerament
diferent, ja que s'alterna la posició del condensador i la resistència.

Per començar les mesures d'interès pràctic en aquesta part es connecta un canal
de l'osci\lgem oscopi entre el condensador i la resistència per mesurar el
voltatge del condensador i es posa el terra després d'aquest. Muntat el circuit
es procedeix a calcular el pseudo-període amb l'osci\lgem oscipi; per tal de
mesurar deu osci\lgem acions del circuit per cada osci\lgem ació del generador
es selecciona una freqüència, calculada prèviament, tal que
\begin{equation}
    f = \frac{1}{10 T} \; .
\end{equation}

A continuació, es canvia la resistència fixa del circuit anterior per una
resistència variable, un potenciòmetre, que va d'entre 0 i 100 k$\Omega$, i es
va variant el seu valor, tenint en compte el valor de la resistència crítica.
El valor de la resistència crítica es troba experimentalment variant el valor
potenciòmetre i visualitzant el senyal per la pantalla de l'oci\lgem oscopi;
quan es té a la pantalla una ona quadrada és perquè s'ha trobat el valor de la
resistència crítica, que seguidament es mesura amb el tester. Aquesta part es
centra en la caracterítzació de les osci\lgem acions en tres regions
determinades per la resistència crítica, per tant, primer s'ajusta una
resistència menor a la crítica i es comprova que l'osci\lgem ació és
subesmorteïda. Per tal de comprovar les osci\lgem acions crítiques es canvia el
valor del potenciòmetre a una resistència igual a la crítica. Finalment es
canvia el valor del potenciòmetre per una resistència major a la crítica i,
novament a través de la pantalla de l'osci\lgem oscopi, s'aprecia una osci\lgem
ació sobreesmorteïda.

\section{Resultats i discussió} \label{RLC:resultats}
En aquesta secció es discuteixen els resultats de l'estudi realitzat sobre el
circuit $RLC$ en sèrie per als dos règims; el règim permanent i el règim transitori. Per
a l'avaluació de les incerteses mostrades consulti's l'apartat
\textit{Avaluació d'incerteses} de l'apèndix.

\paragraph{Règim permanent:}
En haver-se dividit l'estudi del règim permanent en dos experiments, emprant
dues resistències de diferent valor, els resultats d'aquesta primera part es
troben dividits en dues subseccions. Abans de començar, s'ha mesurat el valor
de les resistències experimentalment i s'han avaluat les respectives incerteses
instrumentals a partir del codi cromàtic que aquestes presenten. A la taula
\ref{tab:resultats_R1} es mostren els resultats obtinguts de l'estudi del
circuit al règim permanent per a una resistència de $R= (2700 \pm 135) \,$
$\Omega$.

\begin{longtblr}[theme=Personalitzat,
	label={tab:resultats_R1},
	caption={règim permanent},
	remark{Nota}={Mesures per la resistència de $2700 \unit{\Omega}$.},
	note{*}={Valor sense incertesa.}
		]{
			cells={c},
			row{even}={bg=gray!5},
			row{1}={font=\bfseries},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
	Valor & Teòric & Mesurat \\
	\toprule
	$\mathbf{f_0}$ (\unit{kHz}) & $8,76 \pm 0,34$ & $8,77 \pm 0,10$ \\
	$\mathbf{f_1}$ (\unit{kHz}) & $3,35 \pm 0,38$ & $3,40 \pm 0,10$ \\
	$\mathbf{f_2}$ (\unit{kHz}) & $22,89 \pm 0,41 $ & $22,89 \pm 0,10$ \\
	$\mathbf{\Delta f}$ (\unit{kHz}) & $19,53 \pm 0,10$ & $19,49 \pm 0,10$ \\
	$\mathbf{T_R(\omega)}$ & 1\TblrNote{*} & $0,99 \pm 0,11$ \\
	\bottomrule
\end{longtblr}

En primer lloc, s'observa que el valor de ressonància obtingut de manera
experimental és totalment compatible amb el valor teòric, en efecte, el valor
teòric es troba inclòs dins de l'interval d'incertesa del resultat
experimental. Per altra banda, quan la freqüència d'osci\lgem ació coincideix
amb el valor de la freqüència ressonància, s'espera que el guany en voltatge de
la resistència sigui unitari. Es pot veure llavors que el valor mesurat
d'aquest, tal com s'aprecia a la taula \ref{tab:resultats_R1}, és extremadament
proper a l'esperat i, altra vegada, inclou en el seu interval d'incertesa el
valor teòric corresponent.

Les mesures experimentals corresponents a les freqüències de tall representades
a la taula \ref{tab:resultats_R1} evidencien que els valors adquirits concorden
perfectament amb els que s'havien calculat de manera teòrica, ja que es troben
dins el marge d'incertesa. Endemés, noti's, a les imatges de l'apartat
\textit{Material fotogràfic} de l'Apèndix, que el desfasament entre ambdues
ones, $V_R$ en relació a $V_E$, és aproximadament $\pi/4$ per la primera
freqüència de tall i el mateix valor en negatiu per a la segona freqüència de
tall.

Per a la segona subsecció de la primera part, de manera anàloga a l'apartat
anterior, s'analitza un circuit idèntic però amb una resistència de $R= (270
\pm 10) \Omega$. La totalitat dels resultats pertanyents a aquesta secció de la
pràctica es troben detallats a la taula \ref{tab:resultats_R2}.

\begin{longtblr}[theme=Personalitzat,
	label={tab:resultats_R2},
	caption={règim permanent},
	remark{Nota}={Mesures per la resistència de $270 \unit{\Omega}$.},
	note{*}={Valor sense incertesa.}
		]{
			cells={c},
			row{even}={bg=gray!5},
			row{1}={font=\bfseries},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
	Valor & Teòric & Mesurat \\
	\toprule
	$\mathbf{f_0}$ (\unit{kHz}) & $8,76 \pm 0,34$ & $8,80 \pm 0,10$ \\
	$\mathbf{f_1}$ (\unit{kHz}) & $7,84 \pm 0,38$ & $7,63 \pm 0,10$ \\
	$\mathbf{f_2}$ (\unit{kHz}) & $9,79 \pm 0,41 $ & $10,25 \pm 0,10$ \\
	$\mathbf{\Delta f}$ (\unit{kHz}) & $1,95 \pm 0,10$ & $2,63 \pm 0,20$ \\
	$\mathbf{T_R(\omega)}$ & 1\TblrNote{*} & $0,86 \pm 0,11$ \\
	\bottomrule
\end{longtblr}

Tal i com ha succeït a la subsecció anterior, es pot apreciar que el valor de
la freqüència de ressonància obtingut és totalment coherent respecte del valor
trobat de manera teòrica, atès que és troba dins el marge de la incertesa
d'aquest. Tanmateix, es torna a esperar que el valor experimental de guany de
la resistència sigui unitari quan s'iguala la freqüència d'osci\lgem ació amb
la freqüència de ressonància, no obstant, es pot veure com aquest no és el
resultat obtingut. En efecte, s'observa que els dos valors no són compatibles
dins del rang de les incerteses.

L'explicació d'aquesta situació és que inicialment s'ha assumit que l'únic
component amb resistència és la pròpia resistència, no obstant, això no és
totalment cert. Els altres components del circuit, així com el propi circuit,
també tenen una resistència pròpia interna que s'ha negligit al considerar un
comportament ideal. Dit això, a l'hora de fer els càlculs no s'ha tingut en
compte l'explicació precedent i, per consegüent, el resultat experimental es
veu afectat en comparació amb la predicció teòrica.

Aprofundint, a l'apartat anterior el valor de la pròpia resistència era prou
superior en comparació amb les resistències internes dels altres components del
circuit. Tanmateix, al disminuir un factor deu la resistència, la resta de
fonts de resistència ja no es poden negligir i tenen influència suficient com
per a alterar el valor del guany.

Pel que fa a les freqüències de tall, cap de les dues concorda en totalitat amb
el resultat teòric. Tot i que els marges d'incertesa s'apropen molt no es poden
tractar com a resultats dels quals extreure conclusions sòlides considerant que
els valors nominals se situen fora de l'interval en qüestió. Anàlogament,
aquesta variació apareix perquè es menyspreen les altres resistències del propi
circuit.

Finalment, confirmant la dependència de l'amplada de banda amb la resistència
del circuit, tal com mostra a l'equació (\ref{eq:1}), resulta evident que
aquesta és considerablement inferior respecte a la de l'anterior subsecció. De
manera similar, s'evidència que les freqüències de ressonància depenen de la
magnitud de la resistència.

\paragraph{Règim transitori:}
Un cop preparat el circuit per a la segona part, l'estudi del règim transitori,
s'hi introdueix la freqüència del generador calculada teòricament,
\begin{equation}
    f_{gen} = (4,82 \pm 0,23) \, \text{kHz} \; ,
\end{equation}
i s'obté com a resultat la figura \ref{fig:critica1} de l'Apèndix,
\textit{Material fotogràfic}. On es pot veure clarament una osci\lgem ació
esmorteïda. En aquesta part de la pràctica els valors experimentals dels
components del circuit són
\begin{alignat*}{1}
    R &= (180 \pm 10) \; \Omega \\
    L &= (33 \pm 1) \; \text{mH} \\
    C &= (330 \pm 10) \; \text{pF}
\end{alignat*}
A la taula \ref{tab:resultats_RTrans} de resultats s'observa com el
pseudo-període obtingut experimentalment, tot i diferir lleugerament del valor
teòric esperat, arriba a estar dins dels valors d'incertesa de la predicció
teòrica. No obstant, a la taula \ref{tab:resultats_RTrans} es pot apreciar com
en la resistència crítica, el valor experimental difereix en gran proporció al
valor teòric, cosa que crida molt l'atenció.

\begin{longtblr}[theme=Personalitzat,
	label={tab:resultats_RTrans},
	caption={règim transitori},
	remark{Nota}={Mesures per la resistència de $180 \unit{\Omega}$.},
		]{
			cells={c},
			row{even}={bg=gray!5},
			row{1}={font=\bfseries},
			cell{2-Z}{1}={bg=gray!10},
			rowhead=1
		}
	\toprule
	Valor & Teòric & Mesurat \\
	\toprule
	$\mathbf{T}$ (\unit{\mu.s}) & $20,7 \pm 2,9$ & $20,40 \pm 0,10$ \\
	$\mathbf{R_C}$ (\unit{k\Omega}) & $20,00 \pm 0,30$ & $14,60 \pm 0,10$ \\
	\bottomrule
\end{longtblr}

A l'apartat \textit{Material fotogràfic} de l'apèndix es mostren les captures
de pantalla de l'osci\lgem oscopi on es mostren les osci\lgem acions
tractades en aquesta pràctica, juntament amb comentaris explicatius.

\section{Conclusions} \label{RLC:conclusio}
La realització d'aquesta pràctica ha permès comprendre el funcionament d'un
circuit $RLC$, la inducció del corrent i les osci\lgem acions en aquest. Així
mateix, aquesta pràctica ha permès destacar la importància en electromagnetisme
del circuit estudiat; es tracta del circuit més elemental que presenta aquestes
osci\lgem acions.

Per al règim permanent, s'ha demostrat que la freqüència de ressonància no
depèn de la resistència i que l'aproximació ideal només és vàlida quan la
resistència és major que les resistències internes residuals de la bobina i el
condensador. Si no, els valors teòrics no concorden amb els experimentals, ja
que l'aproximació realitzada no és vàlida. Per a un circuit amb una banda de
pas estreta, és necessari utilitzar resistències petites. En el règim
transitori, s'ha observat el comportament de les ones a l'osci\lgem oscopi
segons la resistència utilitzada i els tres règims diferents: subesmorteït,
crític i sobreesmorteït.

Per tant, es pot concloure que s'han assolit els objectius proposats i s'han
obtingut resultats propers als esperats teòricament, dins del marge de les
aproximacions realitzades, tot i haver vist experimentalment que hi ha valors
que difereixen dels comportaments ideals assumits.
